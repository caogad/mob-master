/*
Navicat Premium Data Transfer

Source Server         : localhost
Source Server Type    : MySQL
Source Server Version : 50726
Source Host           : 127.0.0.1:3306
Source Schema         : scaffolding_separation

Target Server Type    : MySQL
Target Server Version : 50726
File Encoding         : 65001

Date: 01/08/2019 16:18:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
`url` varchar(256) DEFAULT '' COMMENT 'url地址',
`name` varchar(64) DEFAULT NULL COMMENT 'url描述',
`code` varchar(255) DEFAULT NULL COMMENT 'code码',
`type` varchar(255) DEFAULT NULL,
`seq` decimal(6,3) DEFAULT '0.000' COMMENT '排序',
`parent_id` bigint(20) DEFAULT NULL,
`created_time` datetime DEFAULT NULL,
`icon` varchar(255) DEFAULT NULL,
PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of permission
-- ----------------------------
BEGIN;
INSERT INTO `permission` VALUES (1, '', '首页', '', 'menu', 0.000, 0, '2019-07-27 18:41:34', 'dashboard');
INSERT INTO `permission` VALUES (2, '', '系统管理', 'sys:menu', 'menu', 0.001, 0, '2019-07-15 15:11:52', 'table');
INSERT INTO `permission` VALUES (3, '/system/menu', '菜单管理', 'sys:menu', 'menu', 1.000, 2, '2019-07-15 15:13:07', 'tree');
INSERT INTO `permission` VALUES (4, '/system/role', '角色管理', 'sys:role:index', 'menu', 0.000, 2, '2019-07-17 08:31:02', 'user');
INSERT INTO `permission` VALUES (5, '/system/user', '用户管理', 'sys:user:index', 'menu', 0.000, 2, '2019-07-17 14:38:24', 'user');
INSERT INTO `permission` VALUES (6, '/sys/permission/loadPermMenuList', '加载权限菜单', 'sys:permission:loadPermMenuList', 'list', 0.000, 3, '2019-08-01 16:02:07', NULL);
INSERT INTO `permission` VALUES (7, '/sys/permission/detail', '菜单详情', 'sys:permission:detail', 'button', 0.000, 3, '2019-08-01 16:04:44', NULL);
INSERT INTO `permission` VALUES (8, '/sys/permission/save', '新增菜单', 'sys:permission:save', 'button', 0.000, 3, '2019-08-01 13:54:05', NULL);
INSERT INTO `permission` VALUES (9, '/sys/permission/remove', '删除菜单', 'sys:permission:remove', 'button', 0.000, 3, '2019-08-01 13:52:42', NULL);
INSERT INTO `permission` VALUES (10, '/sys/permission/update', '修改菜单', 'sys:permission:update', 'button', 0.000, 3, '2019-08-01 13:53:19', NULL);
INSERT INTO `permission` VALUES (11, '/sys/role/list', '角色列表', 'sys:role:list', 'list', 0.000, 4, '2019-07-23 08:58:07', NULL);
INSERT INTO `permission` VALUES (12, '/sys/role/detail', '角色详情', 'sys:role:detail', 'button', 0.000, 4, '2019-08-01 13:57:42', NULL);
INSERT INTO `permission` VALUES (13, '/sys/role/saveOrUpdate', '角色保存或更新', 'sys:role:saveOrUpdate', 'button', 0.000, 4, '2019-07-23 09:07:21', NULL);
INSERT INTO `permission` VALUES (14, '/sys/role/remove', '删除角色', 'sys:role:remove', 'button', 0.000, 4, '2019-07-23 09:18:17', NULL);
INSERT INTO `permission` VALUES (15, '/sys/permission/loadRolePermTree', '授权列表', 'sys:permission:loadRolePermTree', 'list', 0.000, 4, '2019-07-23 09:24:03', NULL);
INSERT INTO `permission` VALUES (16, '/sys/permission/addPermByRoleId', '提价保存', 'sys:permission:addPermByRoleId', 'button', 0.000, 15, '2019-07-23 09:25:04', NULL);
INSERT INTO `permission` VALUES (17, '/sys/user/list', '用户列表', 'sys:user:list', 'list', 0.000, 5, '2019-07-23 08:53:14', NULL);
INSERT INTO `permission` VALUES (18, '/sys/user/save', '提交保存', 'sys:user:save', 'button', 0.000, 5, '2019-07-23 09:02:58', NULL);
INSERT INTO `permission` VALUES (19, '/sys/user/update', '提交修改', 'sys:user:update', 'button', 0.000, 5, '2019-07-23 09:04:45', NULL);
INSERT INTO `permission` VALUES (20, '/sys/user/remove', '删除用户', 'sys:user:remove', 'button', 0.000, 5, '2019-07-23 09:05:31', NULL);
INSERT INTO `permission` VALUES (21, '/sys/user/detail', '用户详情', 'sys:user:detail', 'button', 0.000, 5, '2019-08-01 14:00:48', NULL);
COMMIT;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
`name` varchar(32) DEFAULT NULL COMMENT '角色名称',
`description` varchar(255) DEFAULT NULL COMMENT '角色描述',
`type` varchar(10) DEFAULT NULL COMMENT '角色类型',
`created_time` datetime DEFAULT NULL COMMENT '创建时间',
`updated_time` datetime DEFAULT NULL COMMENT '更新时间',
PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES (1, 'admin', '管理员', '1', '2019-07-19 14:22:00', '2019-07-30 13:21:24');
COMMIT;

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `rid` bigint(20) DEFAULT NULL COMMENT '角色ID',
`pid` bigint(20) DEFAULT NULL COMMENT '权限ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
BEGIN;
INSERT INTO `role_permission` VALUES (1, 1);
INSERT INTO `role_permission` VALUES (1, 2);
INSERT INTO `role_permission` VALUES (1, 3);
INSERT INTO `role_permission` VALUES (1, 4);
INSERT INTO `role_permission` VALUES (1, 5);
INSERT INTO `role_permission` VALUES (1, 6);
INSERT INTO `role_permission` VALUES (1, 7);
INSERT INTO `role_permission` VALUES (1, 8);
INSERT INTO `role_permission` VALUES (1, 9);
INSERT INTO `role_permission` VALUES (1, 10);
INSERT INTO `role_permission` VALUES (1, 11);
INSERT INTO `role_permission` VALUES (1, 12);
INSERT INTO `role_permission` VALUES (1, 13);
INSERT INTO `role_permission` VALUES (1, 14);
INSERT INTO `role_permission` VALUES (1, 15);
INSERT INTO `role_permission` VALUES (1, 16);
INSERT INTO `role_permission` VALUES (1, 17);
INSERT INTO `role_permission` VALUES (1, 18);
INSERT INTO `role_permission` VALUES (1, 19);
INSERT INTO `role_permission` VALUES (1, 20);
INSERT INTO `role_permission` VALUES (1, 21);
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
`nick_name` varchar(255) DEFAULT NULL,
`email` varchar(255) DEFAULT NULL,
`head_url` varchar(255) DEFAULT NULL,
`account_no` varchar(255) DEFAULT NULL,
`password` varchar(255) DEFAULT NULL,
`token` varchar(255) DEFAULT NULL,
`salt` varchar(255) DEFAULT NULL,
`status` bigint(255) DEFAULT NULL,
`created_time` datetime DEFAULT NULL,
`updated_time` datetime DEFAULT NULL,
`last_login_time` datetime DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'admin', '123@qq.com', NULL, 'admin', 'admin', '1', '1', 1, '2019-07-19 13:29:39', '2019-07-29 09:10:48', '2019-07-19 13:29:46');
COMMIT;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `uid` bigint(20) DEFAULT NULL COMMENT '用户ID',
`rid` bigint(20) DEFAULT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user_role
-- ----------------------------
BEGIN;
INSERT INTO `user_role` VALUES (1, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
