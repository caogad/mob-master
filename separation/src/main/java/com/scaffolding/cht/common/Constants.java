package com.scaffolding.cht.common;

public class Constants {

    public static final String UNAUTHORIZED = "Unauthorized";
    public static final String AUTH = "AUTH";
    public static final String REFRESH = "refresh";
    public static final String LOGIN_SUCCESS = "登录成功";
    public static final String QUERY_SUCCESS = "查询成功";
    public static final String SAVE_SUCCESS = "保存成功";
    public static final String IMPORT_SUCCESS = "数据导入成功";
    public static final String DELETE_SUCCESS = "删除成功";
    public static final String UPDATE_SUCCESS = "更新成功";
}