package com.scaffolding.cht.common;

import com.scaffolding.cht.utils.StringUtil;
import com.scaffolding.cht.validation.ValidateError;
import com.scaffolding.cht.validation.ValidatorFactory;
import com.scaffolding.cht.validation.validator.Validator;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class BaseForm {

    List<ValidateError> errors = new ArrayList();

    public void addError(ValidateError error) {
        this.errors.add(error);
    }

    public void validate() {
        System.out.println();
        Field[] fields = getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                Object value = field.get(this);
                Annotation[] annotations = field.getDeclaredAnnotations();
                for (Annotation annotation : annotations) {
                    Validator validator = ValidatorFactory.create(annotation);
                    if (validator != null) {
                        ValidateError validateError = validator.validate(field, value);
                        if (validateError != null) {
                            this.errors.add(validateError);
                        }
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public void returnErrors(HttpServletRequest request) {
        for (ValidateError error : this.errors) {
            request.setAttribute("error_" + error.field, error.message);
        }
    }

    public boolean hasError() {
        return (this.errors != null) && (!this.errors.isEmpty());
    }

    public <T extends BaseEntity> T toEntity(Class<T> entityClass) {
        T entity = null;
        try {
            entity = (T) entityClass.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        Field[] fields = getClass().getDeclaredFields();
        for (Field field : fields) {
            Field entityField = null;
            try {
                entityField = entityClass.getDeclaredField(field.getName());
                String value = (String) field.get(this);
                if (entityField.getType() == String.class) {
                    entityField.set(entity, value);
                } else if (entityField.getType() == Boolean.class) {
                    entityField.set(entity, Boolean.valueOf(("on".equals(value)) || ("1".equals(value))));
                } else if (entityField.getType() == BigDecimal.class) {
                    if (StringUtil.isNullOrEmpty(value)) {
                        value = "0";
                    }
                    entityField.set(entity, BigDecimal.valueOf(Double.parseDouble(value)));
                } else if (entityField.getType() == Integer.class) {
                    entityField.set(entity, Integer.valueOf(StringUtil.parseInt(value)));
                } else if (entityField.getType() == Long.class) {
                    entityField.set(entity, Long.valueOf(StringUtil.parseLong(value)));
                } else if (entityField.getType() == Double.class) {
                    entityField.set(entity, Double.valueOf(Double.parseDouble(value)));
                } else if (entityField.getType() == Float.class) {
                    entityField.set(entity, Float.valueOf(Float.parseFloat(value)));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        return entity;
    }

    public void fillIn(BaseEntity entity) {
        Field[] fields = getClass().getDeclaredFields();
        for (Field field : fields) {
            Field entityField = null;
            try {
                entityField = entity.getClass().getDeclaredField(field.getName());
                Object value = entityField.get(entity);
                if (entityField.getType() == String.class) {
                    field.set(this, value);
                } else if (entityField.getType() == BigDecimal.class) {
                    field.set(this, String.valueOf(value));
                } else if (entityField.getType() == java.util.Date.class) {
                    field.set(this, StringUtil.formatDate((java.util.Date) value));
                } else if (entityField.getType() == Timestamp.class) {
                    field.set(this, StringUtil.formatDate((Timestamp) value));
                } else if (entityField.getType() == Integer.class) {
                    field.set(this, String.valueOf(value));
                } else if (entityField.getType() == Long.class) {
                    field.set(this, Long.valueOf(StringUtil.parseLong((String) value)));
                } else if (entityField.getType() == Double.class) {
                    field.set(this, String.valueOf(value));
                } else if (entityField.getType() == Float.class) {
                    field.set(this, String.valueOf(value));
                } else if (entityField.getType() == Boolean.class) {
                    field.set(this, StringUtil.parseBooleanValue((Boolean) value));
                }
            } catch (NoSuchFieldException localNoSuchFieldException) {
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public void copyFields(BaseEntity entity) {
        Field[] fields = getClass().getDeclaredFields();
        for (Field field : fields) {
            System.out.println("Copying : " + field.getName());
            Field entityField = null;
            try {
                entityField = entity.getClass().getDeclaredField(field.getName());
                if (!entityField.getName().equals("id")) {
                    Object value = field.get(this);
                    if (value != null) {
                        if (entityField.getType() == String.class) {
                            entityField.set(entity, value);
                        } else if (entityField.getType() == BigDecimal.class) {
                            entityField.set(entity, StringUtil.parseBigDecimal((String) value));
                        } else if (entityField.getType() == java.util.Date.class) {
                            entityField.set(entity, StringUtil.formatDate((java.util.Date) value));
                        } else if (entityField.getType() == Timestamp.class) {
                            entityField.set(entity, StringUtil.formatDate((Timestamp) value));
                        } else if (entityField.getType() == Integer.class) {
                            entityField.set(entity, Integer.valueOf(StringUtil.parseInt((String) value)));
                        } else if (entityField.getType() == Long.class) {
                            entityField.set(entity, Long.valueOf(StringUtil.parseLong((String) value)));
                        } else if (entityField.getType() == Double.class) {
                            entityField.set(entity, Double.valueOf(StringUtil.parseDouble((String) value)));
                        } else if (entityField.getType() == Float.class) {
                            entityField.set(entity, Float.valueOf(StringUtil.parseFloat((String) value)));
                        } else if (entityField.getType() == Boolean.class) {
                            entityField.set(entity, Boolean.valueOf(("on".equals(value)) || ("1".equals(value))));
                        }
                    }
                }
            } catch (NoSuchFieldException localNoSuchFieldException) {
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public String getFirstError() {
        return ((ValidateError) this.errors.get(0)).message;
    }
}
