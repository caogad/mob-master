package com.scaffolding.cht.config;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.scaffolding.cht.annotation.CheckToken;
import com.scaffolding.cht.annotation.PassToken;
import com.scaffolding.cht.common.Constants;
import com.scaffolding.cht.common.ErrorCode;
import com.scaffolding.cht.exception.BusinessException;
import com.scaffolding.cht.utils.JwtTokenUtils;
import com.scaffolding.cht.utils.ResponseUtil;
import com.scaffolding.cht.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
public class MyInterceptor implements HandlerInterceptor {

    private static final String ENCODING = "UTF-8";
    private static final String CONTENT_TYPE = "application/json; charset=utf-8";
    private static final String LOGIN_PATH = "/login";
    private static final String ERROR_PATH = "/error";
    private static final String IDENTITY = "identity";
    private static final String WEBJARS = "webjars";
    private static final String SWAGGER = "swagger";
    private static final String SWAGGER_INDEX = "/doc.html";
    private static final String API = "api";
    private static final String REFRESH_PATH = "/refreshJwt";
    private static final String AUTHORIZATION = "Authorization";
    private static final String PERS = "pers";

    // 在执行目标方法之前执行
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        JwtTokenUtils jwt = new JwtTokenUtils();

        String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");

        InetAddress address = InetAddress.getLocalHost();
        String ip = address.getHostAddress();
        log.info(ip + " 请求: " + path);

        //***********************************************RestFul  API   JWT  Token  使用以下代码************************
        response.setCharacterEncoding(ENCODING);
        response.setContentType(CONTENT_TYPE);

        if (path.equals(LOGIN_PATH) || path.equals(ERROR_PATH) || path.equals(REFRESH_PATH) || path.equals(SWAGGER_INDEX) || path.contains(WEBJARS) || path.contains(SWAGGER) || path.contains(API)) {
            return true;
        }

        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();

        JSONObject res = new JSONObject();
        if (method.isAnnotationPresent(CheckToken.class)) {
            CheckToken userLoginToken = method.getAnnotation(CheckToken.class);
            if (!userLoginToken.required()) {
                log.info("允许未认证访问...");
                return true;
            }
        }

        log.info("开始验证token...");
        // 执行认证
        String token = request.getHeader(AUTHORIZATION);
        if (StringUtil.isNullOrEmpty(token)) {
            return errorMsg(response, res, "-1", Constants.UNAUTHORIZED);
        }

        // 验证 token
        try {
            Map<String, Claim> map = jwt.verifyToken(token);
            Claim pers = map.get(PERS);
            Claim identity = map.get(IDENTITY);
            String perList = pers.asString();

            if (!Constants.AUTH.equals(identity.asString())) {
                response.getWriter().print(ResponseUtil.ERROR(ErrorCode.NOT_AUTH_TOKEN));
                return errorMsg(response, res, ErrorCode.NOT_AUTH_TOKEN + "", ErrorCode.NOT_AUTH_TOKEN.message);
            }

            if (method.isAnnotationPresent(PassToken.class)) {
                log.info("该Method含有PassToken...");
                PassToken passToken = method.getAnnotation(PassToken.class);
                System.err.println("passToken.required() = " + passToken.required());
                if (passToken.required()) {
                    log.info("允许没有权限访问...");
                    return true;
                }
            }
            log.info("开始验证权限...");
            String[] arr = perList.split(",");
            List<String> permissionList = Arrays.asList(arr);
            for (String str : permissionList) {
                if (path.indexOf(str) == 0) {
                    return true;
                }
            }
            return errorMsg(response, res, ErrorCode.HTTP_401.code + "", ErrorCode.HTTP_401.message);
        } catch (BusinessException e) {
            return errorMsg(response, res, String.valueOf(e.code), e.message);
        } catch (Exception e) {
            e.printStackTrace();
            return errorMsg(response, res, "-1", ErrorCode.SYS_ERROR.message);
        }
        //***********************************************RestFul  API   JWT  Token  使用以上代码************************
    }

    private boolean errorMsg(HttpServletResponse response, JSONObject res, String code, String message) throws IOException {
        PrintWriter out;
        res.put("message", message);
        res.put("code", code);
        out = response.getWriter();
        out.append(res.toString());
        return false;
    }

    // 执行目标方法之后执行
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {

    }

    // 在请求已经返回之后执行
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {

    }
}
