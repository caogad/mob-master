package com.scaffolding.cht.validation.validator;

import com.scaffolding.cht.validation.ValidateError;

import java.lang.reflect.Field;

public abstract interface Validator {
    public abstract ValidateError validate(Field paramField, Object paramObject);
}
