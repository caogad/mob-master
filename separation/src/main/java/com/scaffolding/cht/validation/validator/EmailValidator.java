package com.scaffolding.cht.validation.validator;

import com.scaffolding.cht.utils.StringUtil;
import com.scaffolding.cht.validation.ValidateError;
import com.scaffolding.cht.validation.annotation.FieldLabel;

import java.lang.reflect.Field;

public class EmailValidator
        implements Validator {
    public ValidateError validate(Field field, Object value) {
        FieldLabel label = (FieldLabel) field.getAnnotation(FieldLabel.class);
        String text = (String) value;
        if (StringUtil.isNullOrEmpty((String) value)) {
            return null;
        }
        if (!StringUtil.isEmail(text)) {
            return new ValidateError(field, label.value() + "格式不正确");
        }
        return null;
    }
}
