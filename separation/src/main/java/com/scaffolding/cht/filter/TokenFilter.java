//package com.scaffolding.cht.filter;
//
//import com.auth0.jwt.interfaces.Claim;
//import com.scaffolding.cht.common.Constants;
//import com.scaffolding.cht.common.ErrorCode;
//import com.scaffolding.cht.utils.JwtTokenUtils;
//import com.scaffolding.cht.utils.ResponseUtil;
//import com.scaffolding.cht.utils.StringUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.core.annotation.Order;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//
//@Slf4j
//@WebFilter(filterName = "TokenFilter", urlPatterns = "/*")
//@Order(2)
//public class TokenFilter implements Filter {
//
//    private static final String ENCODING = "UTF-8";
//    private static final String LOGIN_PATH = "/login";
//    private static final String IDENTITY = "identity";
//    private static final String WEBJARS = "webjars";
//    private static final String SWAGGER = "swagger";
//    private static final String SWAGGER_INDEX = "/doc.html";
//    private static final String API = "api";
//    private static final String REFRESH_PATH = "/refreshJwt";
//    private static final String AUTHORIZATION = "Authorization";
//    private static final String PERS = "pers";
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//        response.setCharacterEncoding(ENCODING);
//
//        String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");
//
//        if (path.equals(LOGIN_PATH) || path.equals(REFRESH_PATH) || path.equals(SWAGGER_INDEX) || path.contains(WEBJARS) || path.contains(SWAGGER) || path.contains(API)) {
//            filterChain.doFilter(request, response);
//            return;
//        }
//        String token = request.getHeader(AUTHORIZATION);
//        if (StringUtil.isNullOrEmpty(token)) {
//            response.getWriter().print(ResponseUtil.getJSON(ResponseUtil.CODE_401, Constants.UNAUTHORIZED, false));
//            return;
//        }
//
//        JwtTokenUtils jwt = new JwtTokenUtils();
//
//        try {
//
//            Map<String, Claim> map = jwt.verifyToken(token);
//            Claim pers = map.get(PERS);
//            Claim identity = map.get(IDENTITY);
//            String perList = pers.asString();
//            List<String> permissionList = Arrays.asList(perList);
//
//            if (!Constants.AUTH.equals(identity.asString())) {
//                response.getWriter().print(ResponseUtil.ERROR(ErrorCode.NOT_AUTH_TOKEN));
//                return;
//            }
//
//            if (!permissionList.contains(path)) {
//                response.getWriter().print(ResponseUtil.ERROR(ErrorCode.HTTP_401));
//                return;
//            }
//
//            filterChain.doFilter(request, response);
//        } catch (Exception e) {
//            response.getWriter().print(ResponseUtil.ERROR(ErrorCode.LOGIN_AGAIN));
//        }
//    }
//}
