//package com.scaffolding.cht.filter;
//
//import com.auth0.jwt.interfaces.Claim;
//import com.scaffolding.cht.common.Constants;
//import com.scaffolding.cht.utils.JwtTokenUtils;
//import com.scaffolding.cht.utils.ResponseUtil;
//import com.scaffolding.cht.utils.StringUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.core.annotation.Order;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.net.InetAddress;
//import java.util.*;
//
//@Slf4j
//@WebFilter(filterName = "SystemFilter", urlPatterns = "/*")
//@Order(1)
//public class SystemFilter implements Filter {
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//
//        String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");
//
//        InetAddress address = InetAddress.getLocalHost();
//        String ip = address.getHostAddress();
//
//        if (!path.contains("/static") && !path.contains("/favicon.ico")) {
//            log.info(ip + " 请求: " + path);
//        }
//
//        filterChain.doFilter(request, response);
//    }
//}
