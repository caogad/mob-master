package com.scaffolding.cht;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;


@SpringBootApplication
@MapperScan("com.scaffolding.cht.*.mapper")
@EnableCaching
@ServletComponentScan(basePackages = "com.scaffolding.cht")
public class ChtApplication {

	private static final Logger LOGGER = LogManager.getLogger(ChtApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ChtApplication.class, args);
		LOGGER.info("Info level log message");
		LOGGER.debug("Debug level log message");
		LOGGER.error("Error level log message");

	}

}
