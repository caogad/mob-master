package com.scaffolding.cht.sys.service;

import com.alibaba.fastjson.JSONObject;
import com.scaffolding.cht.sys.form.RoleSearchForm;
import com.scaffolding.cht.sys.model.Role;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
public interface RoleService extends IService<Role> {

    JSONObject list(RoleSearchForm form);
}
