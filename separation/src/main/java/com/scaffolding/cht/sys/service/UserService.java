package com.scaffolding.cht.sys.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffolding.cht.sys.form.LoginForm;
import com.scaffolding.cht.sys.model.User;
import com.scaffolding.cht.sys.vo.LoginVo;
import com.scaffolding.cht.sys.vo.UserVo;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
public interface UserService extends IService<User> {

    LoginVo login(LoginForm form);

    JSONObject list(Integer pageSize, Integer pageIndex);

    UserVo getUserVo(Long id);
}
