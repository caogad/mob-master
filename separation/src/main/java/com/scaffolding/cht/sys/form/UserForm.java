package com.scaffolding.cht.sys.form;

import com.google.gson.annotations.Expose;
import com.scaffolding.cht.common.BaseForm;
import com.scaffolding.cht.sys.model.User;
import com.scaffolding.cht.validation.annotation.FieldLabel;
import com.scaffolding.cht.validation.annotation.NotNull;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserForm extends BaseForm {

    @Expose
    @FieldLabel("用户ID")
    @ApiModelProperty(value = "用户ID")
    public Long id;

    @Expose
    @NotNull
    @FieldLabel("邮箱")
    @ApiModelProperty(value = "邮箱", required = true)
    public String email;

    @Expose
    @NotNull
    @FieldLabel("账号")
    @ApiModelProperty(value = "账号", required = true)
    public String accountNo;

    @Expose
    @NotNull
    @FieldLabel("昵称")
    @ApiModelProperty(value = "昵称", required = true)
    public String nickName;

    @Expose
    @NotNull
    @FieldLabel("状态")
    @ApiModelProperty(value = "状态", required = true)
    public String status;

    @Expose
    @NotNull
    @FieldLabel("密码")
    @ApiModelProperty(value = "密码", required = true)
    public String password;

    @Expose
    @NotNull
    @FieldLabel("头像")
    @ApiModelProperty(value = "头像", required = true)
    public String headUrl;

    @Expose
    @NotNull
    @FieldLabel("角色ID")
    @ApiModelProperty(value = "角色ID", required = true)
    public Long roleId;

    public User toUpdateEntity(User user) {
        user.setEmail(this.email);
        user.setPassword(this.password);
        user.setNickName(this.nickName);
        user.setStatus(Long.valueOf(this.status));
        user.setHeadUrl(this.headUrl);
        user.setUpdatedTime(LocalDateTime.now());
        return user;
    }

    public User toSaveEntity() {
        User user = new User();
        user.setEmail(this.email);
        user.setPassword(this.password);
        user.setNickName(this.nickName);
        user.setStatus(Long.valueOf(this.status));
        user.setHeadUrl(this.headUrl);
        user.setCreatedTime(LocalDateTime.now());
        user.setUpdatedTime(LocalDateTime.now());
        return user;
    }
}
