package com.scaffolding.cht.sys.controller;


import com.alibaba.fastjson.JSONObject;
import com.scaffolding.cht.annotation.PassToken;
import com.scaffolding.cht.common.ErrorCode;
import com.scaffolding.cht.common.Result;
import com.scaffolding.cht.common.ResultGenerator;
import com.scaffolding.cht.exception.BusinessException;
import com.scaffolding.cht.sys.form.RoleForm;
import com.scaffolding.cht.sys.form.RoleSearchForm;
import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@RestController
@RequestMapping("/sys/role")
@Api(value = "用户管理", tags = "用户管理")
@ApiResponses({@ApiResponse(code = 400, message = "请求参数没填好"),
        @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对")
})
public class RoleController {

    @Autowired
    public RoleService roleService;

    @PostMapping("/list")
    @PassToken
    @ApiOperation(value = "角色列表", response = Role.class)
    public Result<List<Role>> list(@RequestBody(required = false) RoleSearchForm form) {

        try {
            if (form != null) {
                form.validate();
                if (form.hasError()) {
                    return ResultGenerator.genFailResult(form.getFirstError());
                }
            }
            JSONObject json = roleService.list(form);
            return ResultGenerator.genSuccessResult(json);
        } catch (BusinessException e) {
            return ResultGenerator.genFailResult(e);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(ErrorCode.SYS_ERROR);
        }
    }

    @GetMapping("/detail/{id}")
    @ApiOperation(value = "角色详情", response = Role.class)
    public Result<Role> detail(@PathVariable Long id, HttpServletRequest request) {
        try {
            Role role = roleService.getById(id);
            return ResultGenerator.genSuccessResult(role);
        } catch (Exception e) {
            return ResultGenerator.genFailResult(ErrorCode.SYS_ERROR);
        }
    }

    @PostMapping("/saveOrUpdate")
    @ApiOperation(value = "更新保存角色", response = Result.class)
    public Result save(@RequestBody RoleForm form) {
        try {
            form.validate();
            if (form.hasError()) {
                return ResultGenerator.genFailResult(form.getFirstError());
            }

            roleService.saveOrUpdate(form.toEntity());
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            return ResultGenerator.genFailResult(ErrorCode.SYS_ERROR);
        }
    }

    @PostMapping("/remove/{id}")
    @ApiOperation(value = "删除角色", response = Role.class)
    public Result<Role> remove(@PathVariable Long id) {
        try {
            roleService.removeById(id);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            return ResultGenerator.genFailResult(ErrorCode.SYS_ERROR);
        }
    }
}

