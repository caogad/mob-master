package com.scaffolding.cht.sys.vo;

import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.model.User;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class UserVo {

    public Long id;

    public String nickName;

    public String email;

    public String headUrl;

    public String status;

    public String roleName;

    public Long roleId;

    public LocalDateTime createdTime;

    public LocalDateTime updatedTime;

    public LocalDateTime lastLoginTime;

    public List<Role> roles;

    public UserVo(Map<String, Object> map) {
        this.id = (Long) map.get("id");
        this.nickName = (String) map.get("nickName");
        this.headUrl = (String) map.get("headUrl");
        this.email = (String) map.get("email");
        this.status = String.valueOf(map.get("status"));
        this.roleName = String.valueOf(map.get("roleName"));
        this.roleId = (Long) map.get("roleId");
        this.createdTime = ((Timestamp) map.get("createdTime")).toLocalDateTime();
        this.updatedTime = ((Timestamp) map.get("updatedTime")).toLocalDateTime();
        this.lastLoginTime = ((Timestamp) map.get("lastLoginTime")).toLocalDateTime();
    }
}
