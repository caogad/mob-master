package com.scaffolding.cht.sys.service;

import com.scaffolding.cht.sys.model.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
public interface UserRoleService extends IService<UserRole> {

}
