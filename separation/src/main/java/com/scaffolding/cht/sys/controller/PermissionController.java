package com.scaffolding.cht.sys.controller;


import com.alibaba.fastjson.JSONObject;
import com.baidu.yun.core.annotation.R;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.scaffolding.cht.annotation.CheckToken;
import com.scaffolding.cht.annotation.PassToken;
import com.scaffolding.cht.common.Result;
import com.scaffolding.cht.common.ResultGenerator;
import com.scaffolding.cht.sys.form.MenuForm;
import com.scaffolding.cht.sys.form.PermissionForm;
import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.model.RolePermission;
import com.scaffolding.cht.sys.service.PermissionService;
import com.scaffolding.cht.sys.service.RolePermissionService;
import com.scaffolding.cht.sys.vo.LoginVo;
import com.scaffolding.cht.sys.vo.MenuVo;
import com.scaffolding.cht.utils.JwtTokenUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@RestController
@RequestMapping("/sys/permission")
@Api(value = "权限", tags = "权限")
@ApiResponses({@ApiResponse(code = 400, message = "请求参数没填好"),
        @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对")
})
public class PermissionController {

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RolePermissionService rolePermissionService;

    @GetMapping("/loadMenuUser")
    @ApiOperation(value = "加载左侧菜单栏", response = MenuVo.class)
    @PassToken
    public Result<List<MenuVo>> loadMenuUser(HttpServletRequest request, HttpServletResponse response) {

        JwtTokenUtils jwt = new JwtTokenUtils(request);
        Long userId = jwt.getUserId(jwt.token);
        Set<MenuVo> menuList = permissionService.getMenuList(userId);
        return ResultGenerator.genSuccessResult(menuList);
    }

    @GetMapping("/loadPermMenuList")
    @PassToken
    @ApiOperation(value = "加载权限菜单列表", response = MenuVo.class)
    public Result<List<Permission>> loadPermMenuList() {
        Set<Permission> list = permissionService.loadPermissionTree();
        return ResultGenerator.genSuccessResult(list);
    }

    @ApiOperation(value = "详情", response = Permission.class)
    @GetMapping(value = "/detail/{id}")
    public Result<Permission> detail(@PathVariable Long id) {

        try {
            Permission permission = permissionService.getById(id);
            return ResultGenerator.genSuccessResult(permission);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @ApiOperation(value = "新增权限", response = Result.class)
    @PostMapping(value = "/save")
    public Result save(@RequestBody MenuForm form) {

        try {
            permissionService.save(form.toEntity());
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @ApiOperation(value = "修改权限", response = Result.class)
    @PostMapping(value = "/update")
    public Result update(@RequestBody MenuForm form) {

        try {
            permissionService.updateById(form.toUpdateEntity());
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @ApiOperation(value = "删除权限", response = Result.class)
    @PostMapping(value = "/remove/{id}")
    public Result remove(@PathVariable Long id) {

        try {
            permissionService.removeById(id);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @GetMapping("/loadRolePermTree/{roleId}")
    @ApiOperation(value = "根据角色加载权限树")
    public Result<JSONObject> loadRolePermTree(@PathVariable Long roleId) {
        Set<Permission> list = permissionService.loadPermissionTree();
        List<Long> perms = rolePermissionService.loadPermByRoleId(roleId);
        JSONObject json = new JSONObject();
        json.put("list", list);
        json.put("checked", perms);
        return ResultGenerator.genSuccessResult(json);
    }

    @PostMapping("/addPermByRoleId")
    @ApiOperation(value = "添加权限根据角色")
    public Result<JSONObject> addPermByRoleId(@RequestBody PermissionForm form) {
        try {
            form.validate();
            if (form.hasError()) {
                return ResultGenerator.genFailResult(form.getFirstError());
            }
            QueryWrapper<RolePermission> query = new QueryWrapper<>();
            query.eq("rid", form.roleId);
            rolePermissionService.remove(query);

            List<RolePermission> list = new ArrayList<>();
            RolePermission rolePermission;
            for (Long pid : form.rolePermission) {
                rolePermission = new RolePermission();
                rolePermission.setRid(form.roleId);
                rolePermission.setPid(pid);
                list.add(rolePermission);
            }
            rolePermissionService.saveBatch(list);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }
}

