package com.scaffolding.cht.sys.form;

import com.google.gson.annotations.Expose;
import com.scaffolding.cht.common.BaseForm;
import com.scaffolding.cht.validation.annotation.FieldLabel;
import com.scaffolding.cht.validation.annotation.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class PermissionForm extends BaseForm {

    @Expose
    @FieldLabel("角色ID")
    @NotNull
    @ApiModelProperty(value = "角色ID",required = true)
    public Long roleId;

    @Expose
    @FieldLabel("权限")
    @NotNull
    @ApiModelProperty(value = "权限",required = true)
    public List<Long> rolePermission;
}
