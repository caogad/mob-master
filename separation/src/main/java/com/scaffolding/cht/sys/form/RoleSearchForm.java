package com.scaffolding.cht.sys.form;

import com.google.gson.annotations.Expose;
import com.scaffolding.cht.common.BaseForm;
import com.scaffolding.cht.validation.annotation.FieldLabel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RoleSearchForm extends BaseForm {

    @Expose
    @FieldLabel("当前页数")
    @ApiModelProperty(value = "当前页数", required = true)
    public Integer pageIndex;

    @Expose
    @FieldLabel("分页条数")
    @ApiModelProperty(value = "分页条数", required = true)
    public Integer pageSize;
}
