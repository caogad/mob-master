package com.scaffolding.cht.sys.service.impl;

import com.scaffolding.cht.sys.model.UserRole;
import com.scaffolding.cht.sys.mapper.UserRoleMapper;
import com.scaffolding.cht.sys.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
