package com.scaffolding.cht.sys.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.io.ByteSource;
import com.scaffolding.cht.annotation.CheckToken;
import com.scaffolding.cht.annotation.PassToken;
import com.scaffolding.cht.common.Result;
import com.scaffolding.cht.common.ResultGenerator;
import com.scaffolding.cht.sys.form.UserForm;
import com.scaffolding.cht.sys.form.UserSearchForm;
import com.scaffolding.cht.sys.model.User;
import com.scaffolding.cht.sys.model.UserRole;
import com.scaffolding.cht.sys.service.RoleService;
import com.scaffolding.cht.sys.service.UserRoleService;
import com.scaffolding.cht.sys.service.UserService;
import com.scaffolding.cht.sys.vo.LoginVo;
import com.scaffolding.cht.sys.vo.UserVo;
import com.scaffolding.cht.utils.JwtTokenUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@RestController
@RequestMapping("/sys/user")
@Api(value = "用户管理", tags = "用户管理")
@ApiResponses({@ApiResponse(code = 400, message = "请求参数没填好"),
        @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对")
})
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private RoleService roleService;

    @PostMapping("/list")
    @PassToken
    @ApiOperation(value = "用户列表", response = UserVo.class)
    public Result<JSONObject> list(@RequestBody UserSearchForm form) {
        JSONObject json = userService.list(form.pageSize, form.pageIndex);
        return ResultGenerator.genSuccessResult(json);
    }

    @GetMapping("/detail/{id}")
    @ApiOperation(value = "用户详情", response = UserVo.class)
    public Result<UserVo> detail(@PathVariable Long id, HttpServletRequest request) {
        UserVo user = userService.getUserVo(id);
        return ResultGenerator.genSuccessResult(user);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新用户", response = Result.class)
    public Result update(@RequestBody UserForm form, HttpServletRequest request) {
        try {
            User user = userService.getById(form.id);
            userService.updateById(form.toUpdateEntity(user));
            QueryWrapper query = new QueryWrapper();
            query.ge("uid", form.id);
            userRoleService.remove(query);

            UserRole userRole = new UserRole();
            userRole.setUid(user.getId());
            userRole.setRid(form.roleId);
            userRoleService.save(userRole);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存用户", response = Result.class)
    public Result save(@RequestBody UserForm form) {
        try {
            User user = form.toSaveEntity();
            userService.save(user);

            UserRole userRole = new UserRole();
            userRole.setUid(user.getId());
            userRole.setRid(form.roleId);
            userRoleService.save(userRole);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @PostMapping("/remove/{id}")
    @ApiOperation(value = "删除用户", response = Result.class)
    public Result remove(@PathVariable Long id) {

        try {
            if (id == 1l) {
                return ResultGenerator.genFailResult("超级管理员不允许删除");
            }
            userService.removeById(id);
            QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("uid", id);
            userRoleService.remove(queryWrapper);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }
}