package com.scaffolding.cht.sys.mapper;

import com.scaffolding.cht.sys.form.LoginForm;
import com.scaffolding.cht.sys.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffolding.cht.sys.vo.UserVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    @Select("select * from user where account_no = #{form.accountNo}")
    User findUserByAccountNo(@Param("form") LoginForm form);

    @Select("select u.id as id,u.nick_name as nickName,u.status as status,u.email as email,u.created_time as createdTime,u.last_login_time as lastLoginTime,r.name as roleName,u.updated_time as updatedTime " +
            "from user u " +
            "left join user_role ur on u.id = ur.uid " +
            "left join role r on ur.rid = r.id " +
            "order by u.created_time limit #{pageIndex},#{pageSize}")
    List<Map<String, Object>> list(@Param("pageSize") Integer pageSize, @Param("pageIndex") Integer pageIndex);

    @Select("select count(id) from user")
    Long count();

    @Select("select u.id as id,u.nick_name as nickName,u.email as email,u.head_url as headUrl,u.status as status,r.name as roleName,r.id as roleId,u.created_time as createdTime,u.updated_time as updatedTime,u.last_login_time as lastLoginTime  " +
            "from user u left join user_role ur on u.id = ur.uid " +
            "left join role r on ur.rid = r.id " +
            "where u.id = #{id}")
    Map<String, Object> getUserVo(@Param("id") Long id);
}
