package com.scaffolding.cht.sys.mapper;

import com.scaffolding.cht.sys.model.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

}
