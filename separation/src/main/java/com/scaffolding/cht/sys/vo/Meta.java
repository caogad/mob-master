package com.scaffolding.cht.sys.vo;

import com.scaffolding.cht.sys.model.Permission;

public class Meta {

    public String title;

    public String icon;

    public Meta(Permission permission) {
        this.title = permission.getName();
        this.icon = permission.getIcon();
    }
}
