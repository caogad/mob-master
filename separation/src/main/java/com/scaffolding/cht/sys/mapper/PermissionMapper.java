package com.scaffolding.cht.sys.mapper;

import com.scaffolding.cht.sys.model.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Repository
public interface PermissionMapper extends BaseMapper<Permission> {

    @Select("select group_concat(p.url SEPARATOR ',') from role_permission rp " +
            "left join user_role ur on rp.rid = ur.rid " +
            "left join permission p on rp.pid = p.id " +
            "where ur.uid = #{userId}")
    String getCodeByUserId(Long userId);

    @Select("select * from permission")
    List<Permission> selectAll();
}
