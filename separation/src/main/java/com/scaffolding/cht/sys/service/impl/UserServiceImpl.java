package com.scaffolding.cht.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffolding.cht.common.ErrorCode;
import com.scaffolding.cht.exception.BusinessException;
import com.scaffolding.cht.sys.form.LoginForm;
import com.scaffolding.cht.sys.mapper.PermissionMapper;
import com.scaffolding.cht.sys.mapper.UserMapper;
import com.scaffolding.cht.sys.model.User;
import com.scaffolding.cht.sys.service.UserService;
import com.scaffolding.cht.sys.vo.LoginVo;
import com.scaffolding.cht.sys.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public LoginVo login(LoginForm form) throws BusinessException {

        User user = userMapper.findUserByAccountNo(form);

        if (user == null) {
            throw new BusinessException(ErrorCode.ACCOUNT_NULL);
        }

        if (!form.password.equals(user.getPassword())) {
            throw new BusinessException(ErrorCode.PASSWORD_NOT);
        }

        if (!user.checkStatus()) {
            throw new BusinessException(ErrorCode.FORBID_USER);
        }

        String pers = permissionMapper.getCodeByUserId(user.getId());
        return new LoginVo(user, pers);
    }

    @Override
    public JSONObject list(Integer pageSize, Integer pageIndex) {
        List<Map<String, Object>> list = userMapper.list(pageSize, pageIndex);
        Long totalSize = userMapper.count();
        for (Map<String, Object> map : list) {

            Timestamp createdTime = (Timestamp) map.get("createdTime");
            Timestamp updatedTime = (Timestamp) map.get("updatedTime");
            Timestamp lastLoginTime = (Timestamp) map.get("lastLoginTime");

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            map.put("createdTime", createdTime == null ? null : createdTime.toLocalDateTime().format(formatter));
            map.put("updatedTime", updatedTime == null ? null : updatedTime.toLocalDateTime().format(formatter));
            map.put("lastLoginTime", lastLoginTime == null ? null : lastLoginTime.toLocalDateTime().format(formatter));
        }
        JSONObject json = new JSONObject();
        json.put("list", list);
        json.put("totalSize", totalSize);
        return json;
    }

    @Override
    public UserVo getUserVo(Long id) {
        UserVo vo = new UserVo(userMapper.getUserVo(id));
        return vo;
    }
}
