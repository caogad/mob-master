package com.scaffolding.cht.sys.controller;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.scaffolding.cht.annotation.PassToken;
import com.scaffolding.cht.common.Constants;
import com.scaffolding.cht.common.ErrorCode;
import com.scaffolding.cht.common.Result;
import com.scaffolding.cht.common.ResultGenerator;
import com.scaffolding.cht.exception.BusinessException;
import com.scaffolding.cht.sys.form.LoginForm;
import com.scaffolding.cht.sys.service.UserService;
import com.scaffolding.cht.sys.vo.LoginVo;
import com.scaffolding.cht.utils.DateUtils;
import com.scaffolding.cht.utils.JwtTokenUtils;
import com.scaffolding.cht.utils.ResponseUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@Api(value = "登录", tags = "登录")
@ApiResponses({@ApiResponse(code = 400, message = "请求参数没填好"),
        @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对")
})
public class LoginController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    @ApiOperation(value = "登录API", response = LoginVo.class)
    public Result<LoginVo> login(@RequestBody LoginForm form) {

        try {

            form.validate();
            if (form.hasError()) {
                return ResultGenerator.genFailResult(form.getFirstError());
            }

            LoginVo userInfo = userService.login(form);
            return ResultGenerator.genSuccessResult(userInfo);
        } catch (BusinessException e) {
            return ResultGenerator.genFailResult(e.message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(ErrorCode.SYS_ERROR);
        }
    }

    @GetMapping("/refreshJwt")
    @ApiOperation(value = "刷新JWT")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "needJwt", dataType = "String", required = true, value = "刷新使用的token"),
    })
    public Result refreshJwt(String refreshJwt) {

        try {
            JwtTokenUtils jwt = new JwtTokenUtils();
            Map<String, Claim> map = jwt.verifyToken(refreshJwt);

            Claim userIdClaim = map.get("userId");
            Claim identityClaim = map.get("identity");
            Claim pers = map.get("pers");

            if (null == userIdClaim || null == userIdClaim.asLong()) {
                // token 校验失败, 抛出Token验证非法异常
                throw new BusinessException(ErrorCode.DECRYPT_TOKEN);
            }
            if (!Constants.REFRESH.equals(identityClaim.asString())) {
                // token 校验失败, 抛出Token验证非法异常
                throw new BusinessException(ErrorCode.NOT_REFRESH_TOKEN);
            }

            JSONObject json = new JSONObject();
            json.put("token", jwt.createAuthToken(userIdClaim.asLong(), pers == null ? "[]" : pers.asString()));
            json.put("refresh", jwt.createRefreshToken(userIdClaim.asLong(), pers == null ? "[]" : pers.asString()));
            return ResultGenerator.genSuccessResult(json);
        } catch (BusinessException e) {
            return ResultGenerator.genFailResult(e.message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(ErrorCode.LOGIN_AGAIN);
        }
    }
}
