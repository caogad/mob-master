package com.scaffolding.cht.sys.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户昵称
     */
    private String nickName;

    private String accountNo;

    /**
     * 邮箱|登录帐号
     */
    private String email;

    private String headUrl;

    /**
     * 密码
     */
    private String password;

    private String token;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;

    /**
     * 最后登录时间
     */
    private LocalDateTime lastLoginTime;

    /**
     * 1:有效，0:禁止登录
     */
    private Long status;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getStatusText() {
        return this.status == 1 ? "有效" : "禁用";
    }

    public boolean checkStatus() {
        return this.getStatus() == 1;
    }
}
