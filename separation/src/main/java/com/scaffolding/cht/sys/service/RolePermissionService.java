package com.scaffolding.cht.sys.service;

import com.scaffolding.cht.sys.model.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
public interface RolePermissionService extends IService<RolePermission> {

    List<Long> loadPermByRoleId(Long roleId);
}
