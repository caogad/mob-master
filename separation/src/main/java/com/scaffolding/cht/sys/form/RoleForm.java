package com.scaffolding.cht.sys.form;

import com.baidu.yun.core.annotation.R;
import com.google.gson.annotations.Expose;
import com.scaffolding.cht.common.BaseForm;
import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.validation.annotation.FieldLabel;
import com.scaffolding.cht.validation.annotation.NotNull;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RoleForm extends BaseForm {

    @Expose
    @FieldLabel("角色标识")
    @ApiModelProperty(value = "角色标识")
    public Long id;

    @Expose
    @NotNull
    @FieldLabel("角色名称")
    @ApiModelProperty(value = "角色名称")
    public String name;

    @Expose
    @NotNull
    @FieldLabel("角色描述")
    @ApiModelProperty(value = "角色描述")
    public String description;

    @Expose
    @NotNull
    @FieldLabel("角色类型")
    @ApiModelProperty(value = "角色类型")
    public String type;

    public Role toEntity() {
        Role role = new Role();
        if (this.id != null) {
            role.setId(this.id);
        } else {
            role.setCreatedTime(LocalDateTime.now());
        }
        role.setDescription(this.description);
        role.setName(this.name);
        role.setType(this.type);
        role.setUpdatedTime(LocalDateTime.now());
        return role;
    }
}
