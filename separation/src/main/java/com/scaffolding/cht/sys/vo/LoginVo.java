package com.scaffolding.cht.sys.vo;

import com.scaffolding.cht.common.Constants;
import com.scaffolding.cht.sys.model.User;
import com.scaffolding.cht.utils.DateUtils;
import com.scaffolding.cht.utils.JwtTokenUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "登录信息", value = "UserInfo")
public class LoginVo {

    @ApiModelProperty("token")
    public String token;

    @ApiModelProperty("刷新token")
    public String refreshToken;

    @ApiModelProperty("昵称")
    public String nickName;

    @ApiModelProperty("头像")
    public String headUrl;

    @ApiModelProperty("邮箱")
    public String email;

    @ApiModelProperty("是否禁用(有效/禁用)")
    public String status;

    @ApiModelProperty("创建时间")
    public String createdTime;

    public LoginVo() {
    }

    public LoginVo(User user, String pers) {

        JwtTokenUtils jwt = new JwtTokenUtils();
        this.token = jwt.createAuthToken(user.getId(), pers);
        this.refreshToken = jwt.createRefreshToken(user.getId(), pers);

        this.nickName = user.getNickName();
        this.headUrl = user.getHeadUrl();
        this.email = user.getEmail();
        this.status = user.getStatusText();
        this.createdTime = DateUtils.timeToString(user.getCreatedTime(), DateUtils.YMDHMS);
    }
}
