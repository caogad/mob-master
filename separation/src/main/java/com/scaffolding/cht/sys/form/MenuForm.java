package com.scaffolding.cht.sys.form;

import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.validation.annotation.NotNull;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class MenuForm {

    public Long id;

    public String name;

    public String type;

    public String url;

    public String code;

    public String icon;

    public BigDecimal seq;

    public Long parentId;

    public String createdTime;

    public Permission toEntity() {
        Permission permission = new Permission();
        permission.setCode(this.code);
        permission.setName(this.name);
        permission.setType(this.type);
        permission.setUrl(this.url);
        permission.setSeq(this.seq);
        permission.setIcon(this.icon);
        permission.setParentId(parentId == null || parentId == 0l ? 0l : parentId);
        permission.setCreatedTime(LocalDateTime.now());
        return permission;
    }

    public Permission toUpdateEntity() {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Permission permission = new Permission();
        permission.setId(this.id);
        permission.setCode(this.code);
        permission.setName(this.name);
        permission.setType(this.type);
        permission.setIcon(this.icon);
        permission.setUrl(this.url);
        permission.setSeq(seq);
        this.createdTime = this.createdTime.replace("T", " ");
        permission.setCreatedTime(LocalDateTime.parse(this.createdTime, df));
        return permission;
    }
}
