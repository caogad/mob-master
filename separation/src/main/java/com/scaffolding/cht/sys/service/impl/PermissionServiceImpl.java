package com.scaffolding.cht.sys.service.impl;

import com.scaffolding.cht.sys.mapper.RolePermissionMapper;
import com.scaffolding.cht.sys.mapper.UserRoleMapper;
import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.mapper.PermissionMapper;
import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffolding.cht.sys.vo.MenuVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Override
    public Set<Permission> loadPermissionTree() {
        List<Permission> list = permissionMapper.selectAll();

        Set<Permission> parentList = new TreeSet<>();
        Map<Long, List<Permission>> allMap = new HashMap<>();
        for (Permission permission : list) {

            if (permission.getParentId() == null || permission.getParentId() == 0l) {
                parentList.add(permission);
                continue;
            }

            List<Permission> child = allMap.get(permission.getParentId());
            if (child == null) {
                child = new ArrayList<>();
            }
            child.add(permission);
            allMap.put(permission.getParentId(), child);
        }
        for (Permission permission : parentList) {
            loadChild(allMap, permission);
        }
        return parentList;
    }

    private void loadChild(Map<Long, List<Permission>> allMap, Permission parent) {
        List<Permission> childList = allMap.get(parent.getId());
        if (childList == null || childList.size() == 0) {
            return;
        }
        parent.setChildren(childList);
        for (Permission permission : childList) {
            loadChild(allMap, permission);
        }
    }

    @Override
    public Set<MenuVo> getMenuList(Long userId) {
        if (userId == null) {
            return new TreeSet<>();
        }

        Set<MenuVo> menuList;
        List<Permission> tempList = new ArrayList<>();

        List<Role> roles = userRoleMapper.findRoleByUserId(userId);
        for (Role role : roles) {
            List<Permission> temp = rolePermissionMapper.findMenuByRoleId(role.getId());
            tempList.addAll(temp);
        }

        menuList = convertMenu(tempList);
        return menuList;
    }

    public Set<MenuVo> convertMenu(List<Permission> list) {

        Set<MenuVo> set = new TreeSet<>();
        Map<Long, MenuVo> tempMap = new HashMap<>(256);

        for (Permission permission : list) {
            if ((permission.getParentId() == 0l || permission.getParentId() == null) && !tempMap.containsKey(permission.getId())) {
                tempMap.put(permission.getId(), new MenuVo(permission));
            }
        }
        for (Permission permission : list) {
            if (permission.getParentId() != null && permission.getParentId() != 0l) {
                MenuVo menu = tempMap.get(permission.getParentId());
                if (menu != null) {
                    menu.children.add(new MenuVo(permission));
                    tempMap.put(permission.getParentId(), menu);
                }
            }
        }
        for (Map.Entry<Long, MenuVo> entry : tempMap.entrySet()) {
            MenuVo vo = entry.getValue();
            set.add(vo);
        }
        return set;
    }
}
