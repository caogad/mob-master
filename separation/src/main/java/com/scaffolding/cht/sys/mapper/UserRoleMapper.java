package com.scaffolding.cht.sys.mapper;

import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.model.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 根据用户ID查找角色
     *
     * @param userId
     * @return UserRole
     */
    @Select("SELECT * FROM user_role ur LEFT JOIN role r ON ur.rid = r.id WHERE uid = #{userId}")
    List<UserRole> findUserRoleByUserID(@Param("userId") Long userId);

    @Select("SELECT * FROM user_role ur LEFT JOIN role r ON ur.rid = r.id WHERE ur.uid = #{userId}")
    List<Role> findRoleByUserId(@Param("userId") Long userId);
}
