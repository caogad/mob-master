package com.scaffolding.cht.sys.vo;

import com.scaffolding.cht.sys.model.Permission;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MenuVo implements Comparable<MenuVo> {

    public Long id;

    public String path;

    public boolean hidden = false;

    public BigDecimal seq;

    public Meta meta;

    public List<MenuVo> children = new ArrayList<>();

    public MenuVo(MenuVo vo) {
        this.path = vo.path;
        this.meta = vo.meta;
    }

    public MenuVo(Permission permission) {
        this.id = permission.getId();
        this.path = permission.getUrl();
        this.seq = permission.getSeq();
        this.meta = new Meta(permission);
    }

    @Override
    public int compareTo(MenuVo vo) {
        return this.seq.compareTo(vo.seq);
    }
}
