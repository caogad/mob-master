package com.scaffolding.cht.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baidu.yun.core.annotation.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scaffolding.cht.common.Result;
import com.scaffolding.cht.sys.form.RoleSearchForm;
import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.mapper.RoleMapper;
import com.scaffolding.cht.sys.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public JSONObject list(RoleSearchForm form) {
        List<Role> list;
        JSONObject json = new JSONObject();
        if (form == null) {
            list = roleMapper.selectList(null);
        } else {
            Page<Role> page = new Page<>(form.pageIndex, form.pageSize);
            list = roleMapper.selectPage(page, null).getRecords();
        }

        Integer totalSize = roleMapper.selectCount(null);
        json.put("list", list);
        json.put("totalSize", totalSize);
        return json;
    }
}
