package com.scaffolding.cht.sys.service.impl;

import com.scaffolding.cht.sys.model.RolePermission;
import com.scaffolding.cht.sys.mapper.RolePermissionMapper;
import com.scaffolding.cht.sys.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Override
    public List<Long> loadPermByRoleId(Long roleId) {
        return rolePermissionMapper.loadPermByRoleId(roleId);
    }
}
