package com.scaffolding.cht.sys.form;

import com.google.gson.annotations.Expose;
import com.scaffolding.cht.common.BaseForm;
import com.scaffolding.cht.validation.annotation.FieldLabel;
import com.scaffolding.cht.validation.annotation.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class LoginForm extends BaseForm {

    @Expose
    @FieldLabel("账号")
    @NotNull
    @ApiModelProperty(value = "账号",required = true)
    public String accountNo;

    @Expose
    @FieldLabel("密码")
    @NotNull
    @ApiModelProperty(value = "密码",required = true)
    public String password;
}
