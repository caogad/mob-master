package com.scaffolding.cht.sys.service;

import com.scaffolding.cht.sys.model.Permission;
import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffolding.cht.sys.vo.MenuVo;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author admin
 * @since 2019-07-19
 */
public interface PermissionService extends IService<Permission> {

    Set<Permission> loadPermissionTree();

    Set<MenuVo> getMenuList(Long userId);
}
