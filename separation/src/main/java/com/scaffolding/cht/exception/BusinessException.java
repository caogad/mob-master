package com.scaffolding.cht.exception;

import com.scaffolding.cht.common.ErrorCode;

public class BusinessException extends RuntimeException {

    public Long code;
    public String message;

    public BusinessException() {
    }

    public BusinessException(String message) {
        this.code = null;
        this.message = message;
    }

    public BusinessException(ErrorCode code) {
        this.code = code.code;
        this.message = code.message;
    }

    public String returnError() {
        return this.message;
    }
}
