package com.scaffolding.cht.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.scaffolding.cht.common.Constants;
import com.scaffolding.cht.common.ErrorCode;
import com.scaffolding.cht.config.EnvConfig;
import com.scaffolding.cht.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 生成Token工具类
 * auth:admin
 */
@Configuration
@EnableConfigurationProperties(EnvConfig.class)
public class JwtTokenUtils {

    /**
     * token秘钥，请勿泄露，请勿随便修改
     */
    public static final String SECRET = "cht_0001";
    /**
     * token 过期时间: 10天
     */
    public static final int calendarField = Calendar.DATE;
    public static final int calendarInterval = 10;

    public String token;

    /**
     * 用户登陆后生成Token
     *
     * @param userId
     * @return
     */
    public String createAuthToken(Long userId, String pers) {
        // expire time
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MINUTE, 1000);
        Date expiresDate = nowTime.getTime();

        return createToken(userId, expiresDate, pers, Constants.AUTH);
    }

    /**
     * 用户登陆后使用的刷新Token
     *
     * @param userId
     * @return
     */
    public String createRefreshToken(Long userId, String pers) {
        // expire time
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MINUTE, 100);
        Date expiresDate = nowTime.getTime();

        return createToken(userId, expiresDate, pers, Constants.REFRESH);
    }

    private String createToken(Long userId, Date expiresDate, String pers, String identity) {
        // header Map
        Map<String, Object> map = new HashMap<>();
        map.put("alg", "HS256");
        map.put("typ", "JWT");
        String token = JWT.create()
                .withHeader(map) // header
                .withClaim("iss", "Service") // payload iss: jwt签发者
                .withClaim("aud", "APP")// payload aud: 接收jwt的一方
                .withClaim("identity", identity)// payload aud: 接收jwt的一方
                .withClaim("userId", null == userId ? null : userId)
                .withIssuedAt(new Date()) // sign time
                .withExpiresAt(expiresDate) // expire time
                .withClaim("pers", pers)
                .sign(Algorithm.HMAC256(SECRET)); // 以 SECRET 作为 token 的密钥
        return token;
    }

    /**
     * 解密Token
     *
     * @param token
     * @return
     * @throws Exception
     */
    public Map<String, Claim> verifyToken(String token) {
        DecodedJWT jwt = null;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
            jwt = verifier.verify(token);
        } catch (Exception e) {
//             e.printStackTrace();
            // token 校验失败, 抛出Token验证非法异常
            throw new BusinessException(ErrorCode.DECRYPT_TOKEN);
        }
        return jwt.getClaims();
    }

    /**
     * 根据Token获取user_id
     *
     * @param token
     * @return user_id
     */
    public Long getUserId(String token) {
        Map<String, Claim> claims = verifyToken(token);
        Claim user_id_claim = claims.get("userId");
        return user_id_claim.asLong();
    }

    public JwtTokenUtils() {
    }

    public JwtTokenUtils(HttpServletRequest request) {
        this.token = request.getHeader("Authorization");
    }
}
