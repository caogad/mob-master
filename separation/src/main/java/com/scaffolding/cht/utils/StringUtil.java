package com.scaffolding.cht.utils;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    public static boolean isNullOrEmpty(String toTest) {
        return (toTest == null) || (toTest.length() == 0);
    }

    public static String formatSqlParam(String name) {
        return name.replaceAll("\\.", "_");
    }

    public static String formatSqlField(String name) {
        if (name.startsWith("@")) {
            return name.substring(1);
        }
        return name;
    }

    public static String formatDate(Long date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static String formatDate2(Long date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        return sdf.format(date);
    }

    public static String formatDateTime(Long timeMillis) {
        if (timeMillis == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(timeMillis);
    }

    public static String formatDateTime2(Long timeMillis) {
        if (timeMillis == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(timeMillis);
    }

    public static Long dateToTimeMillis(String dateText) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = sdf.parse(dateText.replaceAll("/", "-"));
            return Long.valueOf(date.getTime());
        } catch (NullPointerException localNullPointerException) {
        } catch (ParseException localParseException) {
        }
        return null;
    }

    public static Long localDateTimeToTimeMillis(String dateText) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = sdf.parse(dateText.replaceAll("/", "-"));
            return Long.valueOf(date.getTime());
        } catch (NullPointerException localNullPointerException) {
        } catch (ParseException localParseException) {
        }
        return null;
    }

    public static Long dateTimeToTimeMillis(String dateText) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            Date date = sdf.parse(dateText.replaceAll("/", "-"));
            return Long.valueOf(date.getTime());
        } catch (NullPointerException localNullPointerException) {
        } catch (ParseException localParseException) {
        }
        return null;
    }

    public static String formatDate(Date value) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(value);
    }

    public static String formatDateTime(Date value) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(value);
    }

    public static String formatDateTimes(Date value) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return sdf.format(value);
    }

    public static String formatDate(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return date.format(formatter);
    }

    public static boolean isObjectField(String field) {
        return field.startsWith("@");
    }

    public static int parseInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
        }
        return 0;
    }

    public static boolean isPhone(String text) {
        String regExp = "((\\d{11})|^((\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1})|(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1}))$)";

        Pattern regex = Pattern.compile(regExp);
        Matcher matcher = regex.matcher(text);
        return matcher.matches();
    }

    public static boolean isMobilePhone(String text) {
        String regExp = "^\\d{11}$";

        Pattern regex = Pattern.compile(regExp);
        Matcher matcher = regex.matcher(text);
        return matcher.matches();
    }

    public static String isString(int number) {
        try {
            return String.valueOf(number);
        } catch (NumberFormatException e) {
        }
        return "";
    }

    public static String isString(double number) {
        try {
            return String.valueOf(number);
        } catch (NumberFormatException e) {
        }
        return "";
    }

    public static String isString(long number) {
        try {
            return String.valueOf(number);
        } catch (NumberFormatException e) {
        }
        return "";
    }

    public static String isString(Float number) {
        try {
            return String.valueOf(number);
        } catch (NumberFormatException e) {
        }
        return "";
    }

    public static String isString(BigDecimal number) {
        try {
            return String.valueOf(number);
        } catch (NumberFormatException e) {
        }
        return "";
    }

    public static long parseLong(String number) {
        try {
            return Long.parseLong(number);
        } catch (NumberFormatException e) {
        }
        return 0L;
    }

    public static double parseDouble(String number) {
        try {
            number = isNullOrEmpty(number) ? "0" : number;
            return Double.parseDouble(number);
        } catch (NumberFormatException e) {
        }
        return 0.0D;
    }

    public static float parseFloat(String number) {
        try {
            return Float.parseFloat(number);
        } catch (NumberFormatException e) {
        }
        return 0.0F;
    }

    public static BigDecimal parseBigDecimal(String number) {
        try {
            return BigDecimal.valueOf(parseDouble(number));
        } catch (NumberFormatException e) {
        }
        return BigDecimal.valueOf(0L);
    }

    public static Boolean parseBoolean(String number) {
        try {
            return Boolean.valueOf(Boolean.parseBoolean(number));
        } catch (NumberFormatException e) {
        }
        return null;
    }

    public static String parseBooleanValue(Boolean value) {
        if (value == null) {
            return "0";
        }
        return value.booleanValue() ? "1" : "0";
    }

    public static boolean isDate(String text) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            simpleDateFormat.parse(text);
            return true;
        } catch (ParseException e) {
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy/MM/dd");
            try {
                simpleDateFormat2.parse(text);
                return true;
            } catch (ParseException e2) {
            }
        }
        return false;
    }

    public static boolean isEmail(String text) {
        String regExp = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

        Pattern regex = Pattern.compile(regExp);
        Matcher matcher = regex.matcher(text);
        return matcher.matches();
    }

    public static boolean isIp(String text) {
        String regExp = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";

        Pattern regex = Pattern.compile(regExp);
        Matcher matcher = regex.matcher(text);
        return matcher.matches();
    }

    public static boolean isUrl(String text) {
        String regExp = "^(https|http)://([\\w-]+\\.)+[\\w-]+(/[\\w-./?%&=]*)?$";

        Pattern regex = Pattern.compile(regExp);
        Matcher matcher = regex.matcher(text);
        return matcher.matches();
    }

    public static boolean isNationalId(String text) {
        String regExp = "^\\d{14}(\\d{1}|\\d{4}|(\\d{3}[xX]))$";
        return text.matches(regExp);
    }

    public static String likeKeyword(String keyword) {
        return "%" + filterNull(keyword) + "%";
    }

    public static String filterNull(String text) {
        if (text == null) {
            return "";
        }
        return text;
    }

    public static boolean validateOrderBy(String sort) {
        if (isNullOrEmpty(sort)) {
            return false;
        }
        String[] segments = sort.split(" ");
        if (segments.length != 2) {
            return false;
        }
        if ((!segments[1].equalsIgnoreCase("asc")) && (!segments[1].equalsIgnoreCase("desc"))) {
            return false;
        }
        if (sort.length() > 20) {
            return false;
        }
        return true;
    }

    public static String decodeUnicode(String theString) {
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len; ) {
            char aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - 48;
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 97;
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 65;
                                break;
                            case ':':
                            case ';':
                            case '<':
                            case '=':
                            case '>':
                            case '?':
                            case '@':
                            case 'G':
                            case 'H':
                            case 'I':
                            case 'J':
                            case 'K':
                            case 'L':
                            case 'M':
                            case 'N':
                            case 'O':
                            case 'P':
                            case 'Q':
                            case 'R':
                            case 'S':
                            case 'T':
                            case 'U':
                            case 'V':
                            case 'W':
                            case 'X':
                            case 'Y':
                            case 'Z':
                            case '[':
                            case '\\':
                            case ']':
                            case '^':
                            case '_':
                            case '`':
                            default:
                                throw new IllegalArgumentException("Malformed   \\uxxxx   encoding.");
                        }
                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't') {
                        aChar = '\t';
                    } else if (aChar == 'r') {
                        aChar = '\r';
                    } else if (aChar == 'n') {
                        aChar = '\n';
                    } else if (aChar == 'f') {
                        aChar = '\f';
                    }
                    outBuffer.append(aChar);
                }
            } else {
                outBuffer.append(aChar);
            }
        }
        return outBuffer.toString();
    }

    public static boolean isDefaultFields(String fieldName) {
        return (fieldName.equals("deleted")) ||
                (fieldName.equals("updatedAt")) ||
                (fieldName.equals("createdAt")) ||
                (fieldName.equals("createdUser")) ||
                (fieldName.equals("createdUser")) ||
                (fieldName.equals("id"));
    }

    public static int getYear(Long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp.longValue());
        return calendar.get(1);
    }

    public static int getMonth(Long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp.longValue());
        return calendar.get(2) + 1;
    }

    public static int getDay(Long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp.longValue());
        return calendar.get(5);
    }

    public static String md5(String text) {
        try {
            byte[] bytes = text.getBytes();
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(bytes);
            byte[] digest = messageDigest.digest();

            return byteArrayToHex(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String byteArrayToHex(byte[] byteArray) {
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        char[] resultCharArray = new char[byteArray.length * 2];

        int index = 0;
        for (byte b : byteArray) {
            resultCharArray[(index++)] = hexDigits[(b >>> 4 & 0xF)];
            resultCharArray[(index++)] = hexDigits[(b & 0xF)];
        }
        return new String(resultCharArray);
    }

    public static String formatFirstLowerCase(String tableName) {
        String name = tableName;
        char charAt = tableName.charAt(0);
        name = Character.toLowerCase(charAt) + name.substring(1, name.length());
        return name;
    }

    public static String formatFirstUpperCase(String tableName) {
        String name = tableName;
        char charAt = tableName.charAt(0);
        name = Character.toUpperCase(charAt) + name.substring(1, name.length());
        return name;
    }

    public static List<String> split(String ids, String s) {
        String[] arr = ids.split(s);
        List<String> list = new ArrayList();
        for (String str : arr) {
            if (!isNullOrEmpty(str)) {
                list.add(str);
            }
        }
        return list;
    }

    public static String splitInBracket(String ids, String s) {
        String[] arr = ids.split(s);
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            str = str + "'" + arr[i] + "',";
        }
        str = str.substring(0, str.length() - 1);
        return str;
    }

    public static String lastIndexOfPie(String text) {
        if (text.lastIndexOf("/") == text.length() - 1) {
            return text;
        }
        return text + "/";
    }
}
