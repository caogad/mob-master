package com.scaffolding.cht.sys.mapper;

import com.scaffolding.cht.sys.model.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffolding.cht.sys.model.RolePermission;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Repository
public interface PermissionMapper extends BaseMapper<Permission> {

    @Select("select * from permission")
    List<Permission> selectAll();

    @Insert("insert into permission(name,code,url,type,seq,parent_id,created_time) values(#{entity.name},#{entity.code},#{entity.url},#{entity.type},#{entity.seq},#{entity.parentId},#{entity.createdTime})")
    boolean save(@Param("entity") Permission entity);

    @Select("select * from permission where id = #{id}")
    Permission findById(@Param("id") Long id);

    @Update("update permission set name=#{entity.name},url=#{entity.url},type=#{entity.type},code=#{entity.code},seq=#{entity.seq},created_time=#{entity.createdTime} where id=#{entity.id}")
    boolean update(@Param("entity") Permission permission);

    @Select("select * from role_permission where rid = #{roleId}")
    List<RolePermission> findByRoleId(@Param("roleId") Long roleId);

    @Delete("delete from role_permission where rid = #{roleId}")
    void removeByRoleId(@Param("roleId") Long roleId);
}
