package com.scaffolding.cht.sys.controller;


import com.alibaba.fastjson.JSONObject;
import com.scaffolding.cht.common.Result;
import com.scaffolding.cht.common.ResultGenerator;
import com.scaffolding.cht.sys.form.UserForm;
import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.model.User;
import com.scaffolding.cht.sys.model.UserRole;
import com.scaffolding.cht.sys.service.RoleService;
import com.scaffolding.cht.sys.service.UserRoleService;
import com.scaffolding.cht.sys.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Controller
@RequestMapping
@Slf4j
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserRoleService userRoleService;

    @RequestMapping({"/login", "/"})
    public String login() {
        return "login";
    }

    @RequiresPermissions("sys:user:index")
    @RequestMapping("sys/user/index")
    public String index() {
        return "user/index";
    }

    @RequiresPermissions("sys:user:add")
    @RequestMapping("sys/user/add")
    public String add(Model model) {

        List<Role> list = roleService.list();
        model.addAttribute("roleLists", list);
        return "user/add";
    }

    @RequiresPermissions("sys:user:edit")
    @RequestMapping("sys/user/edit")
    public String edit(Long id, Model model) {

        List<Role> list = roleService.list();
        User user = userService.getById(id);
        Role role = userRoleService.findRoleByUserId(id).get(0);

        model.addAttribute("user", user);
        model.addAttribute("checkRoleId", role.getId());
        model.addAttribute("roleLists", list);
        return "user/edit";
    }

    @RequestMapping("/logout")
    public String logout() {
        Subject subject = SecurityUtils.getSubject();
        if (subject != null) {
            subject.logout();
        }

        return "login";
    }

    @RequestMapping("/admin")
    @ResponseBody
    public String admin() {
        return "success admin";
    }

    @RequestMapping("/unauthorize")
    public String unauthorize() {
        return "unauthorize";
    }

    @RequestMapping("/auth")
    public String loginUser(@RequestParam("username") String username,
                            @RequestParam("password") String password,
                            HttpSession session,
                            Model model) {
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
            User user = (User) subject.getPrincipal();
            session.setAttribute("user", user);
            model.addAttribute("user", user);

            // 获取用户的可操作菜单
            List<Permission> menuList = userService.getMenuList(user);
            model.addAttribute("menuList", menuList);
            return "index";
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("验证不通过: {}", e.getMessage());
            return "login";
        }
    }

    @ResponseBody
    @RequiresPermissions("sys:user:list")
    @RequestMapping("/sys/user/list")
    public ModelMap list(@RequestParam("pageSize") Integer pageSize,
                         @RequestParam("pageIndex") Integer pageIndex) {
        ModelMap modelMap = userService.list(pageSize, pageIndex);
        return modelMap;
    }

    @ResponseBody
    @RequiresPermissions("sys:user:save")
    @RequestMapping(value = "/sys/user/save", method = RequestMethod.POST)
    public Result save(UserForm userForm) {

        String errorMsg = userForm.check();

        if (StringUtils.isEmpty(errorMsg)) {

            User user = userForm.toEntity();
            user.setPassword(new SimpleHash("MD5", user.getPassword(), ByteSource.Util.bytes(user.getCredentialsSalt()), 2).toString());
            userService.save(user);

            UserRole userRole = new UserRole();
            userRole.setUid(user.getId());
            userRole.setRid(userForm.roleId);
            userRoleService.save(userRole);

            return ResultGenerator.genSuccessResult();
        }

        return ResultGenerator.genFailResult(errorMsg);
    }

    @ResponseBody
    @RequiresPermissions("sys:user:remove")
    @RequestMapping(value = "/sys/user/remove", method = RequestMethod.POST)
    public Result remove(Long id) {

        try {
            userService.removeById(id);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @ResponseBody
    @RequiresPermissions("sys:user:update")
    @RequestMapping(value = "/sys/user/update", method = RequestMethod.POST)
    public Result update(UserForm userForm) {

        try {
            User user = userService.getById(userForm.id);
            if (!user.getPassword().equals(userForm.getPassword())) {
                user = userForm.toEntity();
                user.setPassword(new SimpleHash("MD5", user.getPassword(), ByteSource.Util.bytes(user.getCredentialsSalt()), 2).toString());
            }
            userService.updateById(userForm.toEntity());
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }
}

