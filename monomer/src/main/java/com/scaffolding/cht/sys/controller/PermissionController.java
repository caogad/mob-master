package com.scaffolding.cht.sys.controller;


import com.alibaba.fastjson.JSONObject;
import com.scaffolding.cht.common.Result;
import com.scaffolding.cht.common.ResultGenerator;
import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.model.RolePermission;
import com.scaffolding.cht.sys.service.PermissionService;
import com.scaffolding.cht.sys.service.RolePermissionService;
import com.scaffolding.cht.utils.MenuTreeUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Controller
@RequestMapping("/sys/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RolePermissionService rolePermissionService;

    @RequiresPermissions("sys:permission:rolePerms")
    @RequestMapping("/rolePerms")
    public String rolePerms(Long roleId, Model model) {
        model.addAttribute("roleId", roleId);
        return "role/grant";
    }

    @ResponseBody
    @RequiresPermissions("sys:permission:roleMenuTree")
    @RequestMapping(value = "/roleMenuTree")
    public Result<List<Map<String, Object>>> roleMenuTree(Long id) {

        List<Permission> list = permissionService.list();
        List<RolePermission> rolePermissions = permissionService.selectAllByRoleId(id);
        MenuTreeUtil menuTreeUtil = new MenuTreeUtil(list, rolePermissions);
        List<Map<String, Object>> mapList = menuTreeUtil.buildTree();
        return ResultGenerator.genSuccessResult(mapList);
    }

    @ResponseBody
    @RequiresPermissions("sys:permission:saveRoleMenu")
    @RequestMapping(value = "/saveRoleMenu", method = RequestMethod.POST)
    public Result saveRoleMenu(Long roleId, Long[] menuIds) {
        try {
            if (roleId != null) {
                permissionService.removeByRoleId(roleId);
                if (menuIds != null && menuIds.length > 0) {
                    for (Long menuId : menuIds) {
                        RolePermission roleMenu = new RolePermission();
                        roleMenu.setPid(menuId);
                        roleMenu.setRid(roleId);
                        rolePermissionService.save(roleMenu);
                    }
                }
            }
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }
}

