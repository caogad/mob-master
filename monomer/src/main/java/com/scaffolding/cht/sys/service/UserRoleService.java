package com.scaffolding.cht.sys.service;

import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.model.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface UserRoleService extends IService<UserRole> {

    /**
     * 根据用户ID查找角色
     *
     * @param userId
     * @return UserRole
     */
    List<UserRole> findUserRoleByUserID(Long userId);

    List<Role> findRoleByUserId(Long id);
}
