package com.scaffolding.cht.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.mapper.PermissionMapper;
import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.model.RolePermission;
import com.scaffolding.cht.sys.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<Permission> loadPermissionTree() {
        List<Permission> list = permissionMapper.selectAll();

        List<Permission> parentList = new ArrayList<>();
        Map<Long, List<Permission>> allMap = new HashMap<>();
        for (Permission permission : list) {

            if (permission.getParentId() == null || permission.getParentId() == 0l) {
                parentList.add(permission);
                continue;
            }

            List<Permission> child = allMap.get(permission.getParentId());
            if (child == null) {
                child = new ArrayList<>();
                child.add(permission);
                allMap.put(permission.getParentId(), child);
            } else {
                child.add(permission);
                allMap.put(permission.getParentId(), child);
            }
        }
        list = new ArrayList<>();
        for (Permission permission : parentList) {
            loadChild(allMap, permission, list);
        }
        return list;
    }

    private void loadChild(Map<Long, List<Permission>> allMap, Permission parent, List<Permission> list) {
        list.add(parent);
        List<Permission> childList = allMap.get(parent.getId());
        if (childList == null || childList.size() == 0) {
            return;
        }
        for (Permission permission : childList) {
            loadChild(allMap, permission, list);
        }
    }

    @Override
    public List<Permission> convertMenu(List<Permission> list) {

        List<Permission> permissionList = new ArrayList<>();
        Map<Long, Permission> tempMap = new HashMap<>(256);

        for (Permission permission : list) {
            if ((permission.getParentId() == 0l || permission.getParentId() == null) && !tempMap.containsKey(permission.getId())) {
                tempMap.put(permission.getId(), permission);
            }
        }
        for (Permission permission : list) {
            if (permission.getParentId() != null && permission.getParentId() != 0l) {
                Permission tem = tempMap.get(permission.getParentId());
                if (tem != null) {
                    tem.getChildren().add(permission);
                }
            }
        }
        for (Map.Entry<Long, Permission> entry : tempMap.entrySet()) {
            permissionList.add(entry.getValue());
        }

        return permissionList;
    }

    @Override
    public Permission findById(Long id) {
        Permission permission = permissionMapper.findById(id);
        return permission;
    }

    @Override
    @Transactional
    public boolean save(Permission entity) {
        return permissionMapper.save(entity);
    }

    @Override
    @Transactional
    public boolean update(Permission entity) {
        return permissionMapper.update(entity);
    }

    @Override
    public List<RolePermission> selectAllByRoleId(Long roleId) {
        return permissionMapper.findByRoleId(roleId);
    }

    @Override
    public void removeByRoleId(Long roleId) {
        permissionMapper.removeByRoleId(roleId);
    }
}
