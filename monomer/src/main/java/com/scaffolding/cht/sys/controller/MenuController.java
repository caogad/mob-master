package com.scaffolding.cht.sys.controller;

import com.alibaba.fastjson.JSONObject;
import com.scaffolding.cht.common.Result;
import com.scaffolding.cht.common.ResultGenerator;
import com.scaffolding.cht.sys.form.MenuForm;
import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.service.PermissionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/sys/menu")
public class MenuController {

    @Autowired
    private PermissionService permissionService;

    @RequiresPermissions("sys:menu:index")
    @RequestMapping("/index")
    public String index(Model model) {

        List<Permission> list = permissionService.loadPermissionTree();
        model.addAttribute("list", list);
        return "menu/index";
    }

    @RequiresPermissions("sys:menu:add")
    @RequestMapping("/add")
    public String add(@RequestParam Long parentId, Model model) {
        model.addAttribute("parentId", parentId);
        return "menu/add";
    }

    @RequiresPermissions("sys:menu:edit")
    @RequestMapping("/edit")
    public String edit(@RequestParam Long id, Model model) {
        Permission permission = permissionService.findById(id);
        model.addAttribute("menu", permission);
        return "menu/edit";
    }

    @ResponseBody
    @RequiresPermissions("sys:menu:save")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result save(MenuForm form) {

        try {
            permissionService.save(form.toEntity());
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @ResponseBody
    @RequiresPermissions("sys:menu:update")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result update(MenuForm form) {

        try {
            permissionService.update(form.toUpdateEntity());
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @ResponseBody
    @RequiresPermissions("sys:menu:remove")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Result remove(Long id) {

        try {
            permissionService.removeById(id);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }
}
