package com.scaffolding.cht.sys.form;

import com.scaffolding.cht.common.BaseForm;
import com.scaffolding.cht.sys.model.User;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;

@Data
public class UserForm extends BaseForm {

    public Long id;

    public String nickName;

    public String email;

    public Long status;

    public String password;

    public String salt;

    public Long roleId;

    public User toEntity() {
        User user = new User();
        if (id != null) {
            user.setId(id);
        } else {
            user.setCreatedTime(LocalDateTime.now());
        }
        user.setEmail(this.email);
        user.setNickName(this.nickName);
        user.setPassword(this.password);
        user.setStatus(this.status);
        user.setSalt(this.salt);
        user.setUpdatedTime(LocalDateTime.now());
        return user;
    }

    public String check() {
        if (StringUtils.isEmpty(this.nickName)) {
            return "用户昵称不能为空";
        }
        if (StringUtils.isEmpty(this.email)) {
            return "用户邮箱不能为空";
        }
        if (StringUtils.isEmpty(this.password)) {
            return "密码不能为空";
        }
        if (StringUtils.isEmpty(this.salt)) {
            return "密码盐不能为空";
        }
        if (this.roleId == null) {
            return "请选择角色";
        }
        return null;
    }
}
