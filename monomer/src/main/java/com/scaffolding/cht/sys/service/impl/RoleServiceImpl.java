package com.scaffolding.cht.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.mapper.RoleMapper;
import com.scaffolding.cht.sys.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public ModelMap list(Integer pageSize, Integer pageIndex) {
        List<Role> list = roleMapper.list(pageSize, pageIndex);
        Long totalSize = roleMapper.count();
        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("list", list);
        modelMap.addAttribute("totalSize", totalSize);
        return modelMap;
    }

    @Override
    public Role findById(Long id) {
        return roleMapper.findById(id);
    }

    @Override
    public boolean update(Role entity) {
        return roleMapper.update(entity);
    }

    @Override
    @Transactional
    public boolean save(Role entity) {
        return roleMapper.save(entity);
    }
}
