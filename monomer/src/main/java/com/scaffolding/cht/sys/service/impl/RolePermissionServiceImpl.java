package com.scaffolding.cht.sys.service.impl;

import com.scaffolding.cht.sys.model.RolePermission;
import com.scaffolding.cht.sys.mapper.RolePermissionMapper;
import com.scaffolding.cht.sys.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
