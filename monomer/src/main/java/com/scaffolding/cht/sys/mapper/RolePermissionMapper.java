package com.scaffolding.cht.sys.mapper;

import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.model.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Repository
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

    @Select("select p.id,p.url,p.name,p.code,p.type,p.parent_id,p.seq,p.created_time from role_permission rp " +
            "left join role r on rp.rid = r.id " +
            "left join permission p on rp.pid = p.id " +
            "where r.id = #{roleId} and p.type = 'menu' ")
    List<Permission> findMenuByRoleId(@Param("roleId") Long roleId);

    @Select("select p.id,p.url,p.name,p.code,p.type,p.parent_id,p.seq,p.created_time from role_permission rp " +
            "left join role r on rp.rid = r.id " +
            "left join permission p on rp.pid = p.id " +
            "where r.id = #{roleId} ")
    List<Permission> findPermissionByRoleId(@Param("roleId") Long roleId);
}
