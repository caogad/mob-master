package com.scaffolding.cht.sys.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.scaffolding.cht.common.Result;
import com.scaffolding.cht.common.ResultGenerator;
import com.scaffolding.cht.sys.form.RoleForm;
import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.model.RolePermission;
import com.scaffolding.cht.sys.service.RolePermissionService;
import com.scaffolding.cht.sys.service.RoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Wrapper;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Controller
@RequestMapping("/sys/role")
public class RoleController {

    @Autowired
    private RoleService roleService;
    @Autowired
    private RolePermissionService rolePermissionService;

    @RequiresPermissions("sys:role:index")
    @RequestMapping("/index")
    public String index() {
        return "role/index";
    }

    @RequiresPermissions("sys:role:add")
    @RequestMapping("/add")
    public String add() {
        return "role/add";
    }

    @RequiresPermissions("sys:role:edit")
    @RequestMapping("/edit")
    public String edit(Long id, Model model) {
        Role role = roleService.findById(id);
        model.addAttribute("role", role);
        return "role/edit";
    }

    @ResponseBody
    @RequiresPermissions("sys:role:list")
    @RequestMapping("/list")
    public ModelMap list(@RequestParam("pageSize") Integer pageSize,
                         @RequestParam("pageIndex") Integer pageIndex) {
        ModelMap modelMap = roleService.list(pageSize, pageIndex);
        return modelMap;
    }

    @ResponseBody
    @RequiresPermissions("sys:role:save")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result save(RoleForm roleForm) {

        try {
            roleService.save(roleForm.toEntity());
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @ResponseBody
    @RequiresPermissions("sys:role:update")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result update(RoleForm roleForm) {

        try {
            roleService.update(roleForm.toEntity());
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @ResponseBody
    @RequiresPermissions("sys:role:remove")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Result remove(Long id) {

        try {
            roleService.removeById(id);
            QueryWrapper<RolePermission> query = new QueryWrapper<>();
            query.ge("rid", id);
            rolePermissionService.remove(query);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }
}

