package com.scaffolding.cht.sys.service.impl;

import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.model.UserRole;
import com.scaffolding.cht.sys.mapper.UserRoleMapper;
import com.scaffolding.cht.sys.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public List<UserRole> findUserRoleByUserID(Long userId) {
        return userRoleMapper.findUserRoleByUserID(userId);
    }

    @Override
    public List<Role> findRoleByUserId(Long id) {
        return userRoleMapper.findRoleByUserId(id);
    }
}
