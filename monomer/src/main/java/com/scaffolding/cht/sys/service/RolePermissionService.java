package com.scaffolding.cht.sys.service;

import com.scaffolding.cht.sys.model.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface RolePermissionService extends IService<RolePermission> {

}
