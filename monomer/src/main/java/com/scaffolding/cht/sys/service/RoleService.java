package com.scaffolding.cht.sys.service;

import com.scaffolding.cht.sys.model.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.ui.ModelMap;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface RoleService extends IService<Role> {

    boolean save(Role role);

    ModelMap list(Integer pageSize, Integer pageIndex);

    Role findById(Long id);

    boolean update(Role entity);
}
