package com.scaffolding.cht.sys.form;

import com.scaffolding.cht.sys.model.Role;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RoleForm {

    public Long id;

    public String name;

    public String description;

    public Role toEntity() {
        Role role = new Role();
        if (id != null) {
            role.setId(id);
        } else {
            role.setCreatedTime(LocalDateTime.now());
        }
        role.setName(name);
        role.setDescription(description);
        role.setUpdatedTime(LocalDateTime.now());
        return role;
    }
}
