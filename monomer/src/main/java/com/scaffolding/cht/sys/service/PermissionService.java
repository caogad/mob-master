package com.scaffolding.cht.sys.service;

import com.scaffolding.cht.sys.model.Permission;
import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffolding.cht.sys.model.RolePermission;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface PermissionService extends IService<Permission> {

    List<Permission> loadPermissionTree();

    List<Permission> convertMenu(List<Permission> list);

    Permission findById(Long id);

    boolean update(Permission entity);

    List<RolePermission> selectAllByRoleId(Long roleId);

    void removeByRoleId(Long roleId);
}
