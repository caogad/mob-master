package com.scaffolding.cht.sys.mapper;

import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据用户名查找用户
     *
     * @param userName 用户名
     * @return user
     */
    @Select("SELECT * FROM user where nick_name = #{userName}")
    User findByUserName(@Param("userName") String userName);

    @Select("select u.id as uid,u.nick_name as nickName,u.status as status,u.email as email,u.created_time as createdTime,u.last_login_time as lastLoginTime,r.name as roleName,u.updated_time as updatedTime " +
            "from user u " +
            "left join user_role ur on u.id = ur.uid " +
            "left join role r on ur.rid = r.id " +
            "order by u.created_time limit #{pageIndex},#{pageSize}")
    List<Map<String, Object>> list(@Param("pageSize") Integer pageSize, @Param("pageIndex") Integer pageIndex);

    @Select("select count(id) from user")
    Long count();
}
