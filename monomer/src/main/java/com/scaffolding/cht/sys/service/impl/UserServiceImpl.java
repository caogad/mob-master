package com.scaffolding.cht.sys.service.impl;

import com.scaffolding.cht.sys.mapper.RolePermissionMapper;
import com.scaffolding.cht.sys.mapper.UserRoleMapper;
import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.model.Role;
import com.scaffolding.cht.sys.model.User;
import com.scaffolding.cht.sys.mapper.UserMapper;
import com.scaffolding.cht.sys.service.PermissionService;
import com.scaffolding.cht.sys.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private RolePermissionMapper rolePermissionMapper;
    @Autowired
    private PermissionService permissionService;

    @Override
    public User findByUserName(String username) {
        return userMapper.findByUserName(username);
    }

    @Override
    public List<Permission> getMenuList(User user) {

        if (user == null) {
            return new ArrayList<>();
        }

        List<Permission> menuList = new ArrayList<>();

        List<Role> roles = userRoleMapper.findRoleByUserId(user.getId());
        for (Role role : roles) {
            List<Permission> temp = rolePermissionMapper.findMenuByRoleId(role.getId());
            menuList.addAll(temp);
        }

        menuList = permissionService.convertMenu(menuList);
        return menuList;
    }

    @Override
    public Set<Permission> getAllPermissionByUser(User user) {
        List<Role> roles = userRoleMapper.findRoleByUserId(user.getId());
        Set<Permission> permissions = new HashSet<>();
        for (Role role : roles) {
            List<Permission> temp = rolePermissionMapper.findPermissionByRoleId(role.getId());
            permissions.addAll(temp);
        }
        return permissions;
    }

    @Override
    public ModelMap list(Integer pageSize, Integer pageIndex) {
        List<Map<String, Object>> list = userMapper.list(pageSize, pageIndex);
        Long totalSize = userMapper.count();
        for (Map<String, Object> map : list) {

            Timestamp createdTime = (Timestamp) map.get("createdTime");
            Timestamp updatedTime = (Timestamp) map.get("updatedTime");
            Timestamp lastLoginTime = (Timestamp) map.get("lastLoginTime");

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            map.put("createdTime", createdTime == null ? null : createdTime.toLocalDateTime().format(formatter));
            map.put("updatedTime", updatedTime == null ? null : updatedTime.toLocalDateTime().format(formatter));
            map.put("lastLoginTime", lastLoginTime == null ? null : lastLoginTime.toLocalDateTime().format(formatter));
        }
        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("list", list);
        modelMap.addAttribute("totalSize", totalSize);
        return modelMap;
    }
}
