package com.scaffolding.cht.sys.mapper;

import com.scaffolding.cht.sys.model.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

    @Select("select * from role order by created_time limit #{pageIndex},#{pageSize}")
    List<Role> list(Integer pageSize, Integer pageIndex);

    @Select("select count(id) from role")
    Long count();

    @Insert("insert into role(name,description,created_time,updated_time) values (#{role.name},#{role.description},#{role.createdTime},#{role.updatedTime})")
    boolean save(@Param("role") Role entity);

    @Select("select * from role where id = #{id}")
    Role findById(@Param("id") Long id);

    @Update("update role set name=#{role.name},description=#{role.description},updated_time=#{role.updatedTime} where id = #{role.id}")
    boolean update(@Param("role") Role role);
}
