package com.scaffolding.cht.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.model.User;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface UserService extends IService<User> {

    /**
     * 根据用户名查找用户
     *
     * @param username 用户名
     * @return user
     */
    com.scaffolding.cht.sys.model.User findByUserName(String username);

    List<Permission> getMenuList(User user);

    Set<Permission> getAllPermissionByUser(User user);

    ModelMap list(Integer pageSize, Integer pageIndex);
}
