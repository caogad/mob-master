package com.scaffolding.cht.utils;

import com.alibaba.fastjson.JSONObject;

import java.util.Set;

import com.scaffolding.cht.common.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResponseUtil {
    private static final Logger logger = LoggerFactory.getLogger(ResponseUtil.class);
    public static final String CODE_200 = "200";
    public static final String CODE_404 = "404";
    public static final String CODE_405 = "405";
    public static final String CODE_500 = "500";
    public static final boolean CORRECT = true;
    public static final boolean DEFEATED = false;

    public static JSONObject OK(String message, JSONObject jsonObject) {
        JSONObject json = getJSON("200", message, true);
        json.put("data", jsonObject);
        return json;
    }

    public static JSONObject OK(BaseEntity baseEntity) {
        JSONObject json = getJSON("200", null, true);
        json.put("data", baseEntity);
        return json;
    }

    public static JSONObject OK(String message) {
        JSONObject json = getJSON("200", message, true);
        return json;
    }

    public static JSONObject OK(String message, Set<?> resultTreeSet) {
        JSONObject json = getJSON("200", message, true);
        json.put("data", resultTreeSet);
        return json;
    }

    public static JSONObject OK(String message, BaseEntity baseEntity) {
        JSONObject json = getJSON("200", message, true);
        json.put("data", baseEntity);
        return json;
    }

    public static JSONObject ERROR(String error) {
        JSONObject json = getJSON("405", error, false);
        return json;
    }

    private static JSONObject getJSON(String code, String message, boolean success) {
        JSONObject json = new JSONObject();
        json.put("success", Boolean.valueOf(success));
        json.put("message", message);
        json.put("code", code);
        return json;
    }
}
