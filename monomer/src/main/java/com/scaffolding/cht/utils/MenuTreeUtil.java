package com.scaffolding.cht.utils;

import com.scaffolding.cht.sys.model.Permission;
import com.scaffolding.cht.sys.model.RolePermission;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class MenuTreeUtil {

    private List<Permission> nodes;

    private List<RolePermission> checknodes;

    /**
     * 创建一个新的实例 Tree.
     *
     * @param nodes 将树的所有节点都初始化进来。
     */
    public MenuTreeUtil(List<Permission> nodes, List<RolePermission> checknodes) {
        this.nodes = nodes;
        this.checknodes = checknodes;
    }

    /**
     * buildTree
     * 描述:  创建树
     *
     * @return List<Map < String, Object>>
     * @throws
     * @since 1.0.0
     */
    public List<Map<String, Object>> buildTree() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (Permission node : nodes) {
            //这里判断父节点，需要自己更改判断
            if (node.getParentId() == null || node.getParentId() == 0l) {
                Map<String, Object> map = buildTreeChildsMap(node);
                list.add(map);
            }
        }
        return list;
    }

    /**
     * buildChilds
     * 描述:  创建树下的节点。
     *
     * @param node
     * @return List<Map < String, Object>>
     * @throws
     * @since 1.0.0
     */
    private List<Map<String, Object>> buildTreeChilds(Permission node) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        List<Permission> childNodes = getChilds(node);
        for (Permission childNode : childNodes) {
            Map<String, Object> map = buildTreeChildsMap(childNode);
            list.add(map);
        }
        return list;
    }

    /**
     * buildChildMap
     * 描述:生成Map节点
     *
     * @param childNode
     * @return Map<String, Object>
     */
    private Map<String, Object> buildTreeChildsMap(Permission childNode) {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> stateMap = new HashMap<>();
        stateMap.put("checked", false);
        for (RolePermission checknode : checknodes) {
            if (checknode.getPid().equals(childNode.getId())) {
                stateMap.put("checked", true);
            }
        }
        stateMap.put("disabled", false);
        stateMap.put("expanded", false);
        stateMap.put("selected", false);
        map.put("id", childNode.getId());
        map.put("text", childNode.getName());
        map.put("url", childNode.getUrl());
        map.put("state", stateMap);
        List<Map<String, Object>> childs = buildTreeChilds(childNode);
        if (!childs.isEmpty() && childs.size() != 0) {
            map.put("nodes", childs);
        }
        return map;
    }


    /**
     * getChilds
     * 描述:  获取子节点
     *
     * @param parentNode
     * @return List<Resource>
     * @throws
     * @since 1.0.0
     */
    public List<Permission> getChilds(Permission parentNode) {
        List<Permission> childNodes = new ArrayList<Permission>();
        for (Permission node : nodes) {
            if (node.getParentId() == parentNode.getId()) {
                childNodes.add(node);
            }
        }
        return childNodes;
    }

    /**
     * buildTree
     * 描述:  创建树
     *
     * @return List<Map < String, Object>>
     * @throws
     * @since 1.0.0
     */
    public List<Permission> buildTreeGrid() {
        List<Permission> list = new ArrayList<Permission>();
        for (Permission node : nodes) {
            //这里判断父节点，需要自己更改判断
            if (node.getParentId() == null || node.getParentId() == 0l) {
                List<Permission> childs = buildTreeGridChilds(node);
                node.setChildren(childs);
                list.add(node);
            }
        }
        return list;
    }

    /**
     * buildChilds
     * 描述:  创建树下的节点。
     *
     * @param node
     * @return List<Map < String, Object>>
     * @throws
     * @since 1.0.0
     */
    private List<Permission> buildTreeGridChilds(Permission node) {
        List<Permission> list = new ArrayList<Permission>();
        List<Permission> childNodes = getChilds(node);
        for (Permission childNode : childNodes) {
            List<Permission> childs = buildTreeGridChilds(childNode);
            childNode.setChildren(childs);
            list.add(childNode);
        }
        return list;
    }


}
