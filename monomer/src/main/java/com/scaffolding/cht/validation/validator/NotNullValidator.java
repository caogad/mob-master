package com.scaffolding.cht.validation.validator;

import com.scaffolding.cht.validation.ValidateError;
import com.scaffolding.cht.validation.annotation.FieldLabel;

import java.lang.reflect.Field;

public class NotNullValidator implements Validator {
    public ValidateError validate(Field field, Object value) {
        FieldLabel label = (FieldLabel) field.getAnnotation(FieldLabel.class);
        if (value == null) {
            return new ValidateError(field, "请输入" + label.value());
        }
        if ((field.getType() == String.class) && (value.equals(""))) {
            return new ValidateError(field, "请输入" + label.value());
        }
        return null;
    }
}