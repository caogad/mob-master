package com.scaffolding.cht.validation;

import com.scaffolding.cht.validation.annotation.NotNull;
import com.scaffolding.cht.validation.validator.NotNullValidator;
import com.scaffolding.cht.validation.validator.Validator;

import java.lang.annotation.Annotation;

public class ValidatorFactory {
    public static Validator create(Annotation annotation) {
        if ((annotation instanceof NotNull)) {
            return new NotNullValidator();
        }
        return null;
    }
}
