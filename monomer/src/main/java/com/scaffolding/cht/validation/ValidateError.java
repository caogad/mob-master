package com.scaffolding.cht.validation;

import com.google.gson.annotations.Expose;

import java.lang.reflect.Field;

public class ValidateError {
    @Expose
    public String field;
    @Expose
    public String message;

    public ValidateError(Field field, String msg) {
        this.field = field.getName();
        this.message = msg;
    }

    public ValidateError(String fieldName, String msg) {
        this.field = fieldName;
        this.message = msg;
    }

    public String toString() {
        return this.message;
    }
}
