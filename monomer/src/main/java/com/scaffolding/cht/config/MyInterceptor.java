package com.scaffolding.cht.config;

import com.alibaba.fastjson.JSONObject;
import com.scaffolding.cht.annotation.CheckToken;
import com.scaffolding.cht.annotation.PassToken;
import com.scaffolding.cht.utils.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;

@Slf4j
public class MyInterceptor implements HandlerInterceptor {

    @Autowired
    private JwtTokenUtils jwtTokenUtils;

    // 在执行目标方法之前执行
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");

        InetAddress address = InetAddress.getLocalHost();
        String ip = address.getHostAddress();

        if (!path.contains("/static") && !path.contains("/favicon.ico")) {
            log.info(ip + " 请求: " + path);
        }

        //进行逻辑判断，如果ok就返回true，不行就返回false，返回false就不会处理改请求
        return true;
    }

    private boolean errorMsg(HttpServletResponse response, JSONObject res, String message) throws IOException {
        PrintWriter out;
        res.put("message", message);
        res.put("code", "-1");
        out = response.getWriter();
        out.append(res.toString());
        return false;
    }

    // 执行目标方法之后执行
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {

    }

    // 在请求已经返回之后执行
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }
}
