/*
Navicat Premium Data Transfer

Source Server         : localhost
Source Server Type    : MySQL
Source Server Version : 50726
Source Host           : 127.0.0.1:3306
Source Schema         : scaffolding

Target Server Type    : MySQL
Target Server Version : 50726
File Encoding         : 65001

Date: 23/07/2019 09:28:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
`url` varchar(256) DEFAULT NULL COMMENT 'url地址',
`name` varchar(64) DEFAULT NULL COMMENT 'url描述',
`code` varchar(255) DEFAULT NULL COMMENT 'code码',
`type` varchar(255) DEFAULT NULL,
`seq` double(255,0) DEFAULT NULL COMMENT '排序',
`parent_id` bigint(20) DEFAULT NULL,
`created_time` datetime DEFAULT NULL,
PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of permission
-- ----------------------------
BEGIN;
INSERT INTO `permission` VALUES (6, '', '系统管理', 'sys:menu', 'menu', 0, 0, '2019-07-15 15:11:52');
INSERT INTO `permission` VALUES (7, '/sys/menu/index', '菜单管理', 'sys:menu:index', 'menu', 1, 6, '2019-07-15 15:13:07');
INSERT INTO `permission` VALUES (19, '/sys/role/index', '角色管理', 'sys:role:index', 'menu', 0, 6, '2019-07-17 08:31:02');
INSERT INTO `permission` VALUES (20, '/sys/user/index', '用户管理', 'sys:user:index', 'menu', 0, 6, '2019-07-17 14:38:24');
INSERT INTO `permission` VALUES (21, '/sys/user/list', '用户列表', 'sys:user:list', 'list', 0, 20, '2019-07-23 08:53:14');
INSERT INTO `permission` VALUES (22, '/sys/role/list', '角色列表', 'sys:role:list', 'list', 0, 19, '2019-07-23 08:58:07');
INSERT INTO `permission` VALUES (23, '/sys/user/add', '添加用户', 'sys:user:add', 'button', 0, 20, '2019-07-23 09:00:39');
INSERT INTO `permission` VALUES (24, '/sys/user/add', '提交保存', 'sys:user:save', 'button', 0, 23, '2019-07-23 09:02:58');
INSERT INTO `permission` VALUES (26, '/sys/user/edit', '用户编辑', 'sys:user:edit', 'button', 0, 20, '2019-07-23 09:04:20');
INSERT INTO `permission` VALUES (27, '/sys/user/update', '提交修改', 'sys:user:update', 'button', 0, 26, '2019-07-23 09:04:45');
INSERT INTO `permission` VALUES (28, '/sys/user/remove', '删除用户', 'sys:user:remove', 'button', 0, 20, '2019-07-23 09:05:31');
INSERT INTO `permission` VALUES (29, '/sys/role/add', '添加角色', 'sys:role:add', 'button', 0, 19, '2019-07-23 09:06:40');
INSERT INTO `permission` VALUES (30, '/sys/role/save', '提交保存', 'sys:role:save', 'button', 0, 29, '2019-07-23 09:07:21');
INSERT INTO `permission` VALUES (31, '/sys/role/edit', '角色编辑', 'sys:role:edit', 'button', 0, 19, '2019-07-23 09:08:06');
INSERT INTO `permission` VALUES (32, '/sys/role/update', '提交修改', 'sys:role:update', 'button', 0, 31, '2019-07-23 09:08:31');
INSERT INTO `permission` VALUES (33, '/sys/role/remove', '删除角色', 'sys:role:remove', 'button', 0, 19, '2019-07-23 09:18:17');
INSERT INTO `permission` VALUES (34, '/sys/permission/rolePerms', '授权', 'sys:permission:rolePerms', 'button', 0, 19, '2019-07-23 09:20:15');
INSERT INTO `permission` VALUES (35, '/sys/permission/roleMenuTree', '授权列表', 'sys:permission:roleMenuTree', 'auth', 0, 19, '2019-07-23 09:24:03');
INSERT INTO `permission` VALUES (36, '/sys/permission/saveRoleMenu', '提交保存', 'sys:permission:saveRoleMenu', 'menu', 0, 35, '2019-07-23 09:25:04');
COMMIT;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
`name` varchar(32) DEFAULT NULL COMMENT '角色名称',
`description` varchar(255) DEFAULT NULL COMMENT '角色描述',
`type` varchar(10) DEFAULT NULL COMMENT '角色类型',
`created_time` datetime DEFAULT NULL COMMENT '创建时间',
`updated_time` datetime DEFAULT NULL COMMENT '更新时间',
PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES (1, 'admin', '管理员', '1', '2019-07-17 09:20:22', '2019-07-17 10:46:08');
COMMIT;

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `rid` bigint(20) DEFAULT NULL COMMENT '角色ID',
`pid` bigint(20) DEFAULT NULL COMMENT '权限ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
BEGIN;
INSERT INTO `role_permission` VALUES (1, 6);
INSERT INTO `role_permission` VALUES (1, 7);
INSERT INTO `role_permission` VALUES (1, 19);
INSERT INTO `role_permission` VALUES (1, 20);
INSERT INTO `role_permission` VALUES (1, 21);
INSERT INTO `role_permission` VALUES (1, 22);
INSERT INTO `role_permission` VALUES (1, 23);
INSERT INTO `role_permission` VALUES (1, 24);
INSERT INTO `role_permission` VALUES (1, 26);
INSERT INTO `role_permission` VALUES (1, 27);
INSERT INTO `role_permission` VALUES (1, 28);
INSERT INTO `role_permission` VALUES (1, 29);
INSERT INTO `role_permission` VALUES (1, 30);
INSERT INTO `role_permission` VALUES (1, 31);
INSERT INTO `role_permission` VALUES (1, 32);
INSERT INTO `role_permission` VALUES (1, 33);
INSERT INTO `role_permission` VALUES (1, 34);
INSERT INTO `role_permission` VALUES (1, 35);
INSERT INTO `role_permission` VALUES (1, 36);
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
`nick_name` varchar(20) DEFAULT NULL COMMENT '用户昵称',
`email` varchar(128) DEFAULT NULL COMMENT '邮箱|登录帐号',
`password` varchar(32) DEFAULT NULL COMMENT '密码',
`created_time` datetime DEFAULT NULL COMMENT '创建时间',
`updated_time` datetime DEFAULT NULL,
`last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
`status` bigint(1) DEFAULT '1' COMMENT '1:有效，0:禁止登录',
`salt` varchar(32) DEFAULT NULL COMMENT '盐',
PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'admin', '123@qq.com', 'c5941c5f3bc693a75e6e863bd2c55ce3', '2017-05-10 20:22:59', '2019-07-17 14:59:22', '2019-07-17 14:46:09', 1, '1ab6d62faa91ae7deec76d6f13ef1600');
COMMIT;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `uid` bigint(20) DEFAULT NULL COMMENT '用户ID',
`rid` bigint(20) DEFAULT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user_role
-- ----------------------------
BEGIN;
INSERT INTO `user_role` VALUES (1, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
